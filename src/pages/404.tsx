import React from 'react';
import { Flex, Box } from 'reflexbox';
import styled from 'styled-components';
import Layout from '../components/Layout';
import SEO from '../components/SEO';
import Card from '../components/Card';
import pxToRem from '../utils/pxToRem';
import Loading from './Loading';
import defatulSocialImage from '../images/uno-shared-default.jpg';

const Page404HeroWrapper = styled.div`
  padding: ${(props) => props.theme.spacing.spacingThree} 0
    ${(props) => props.theme.spacing.spacingThree};
  text-align: center;
  background: linear-gradient(
    -45deg,
    rgb(93, 140, 150) 0%,
    rgb(46, 62, 67) 100%
  );
`;

const Page404HeroWrapperContent = styled.div`
  padding: 0;
  color: ${(props) => props.theme.colors.colorWhite};

  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingFive} 0;
  }

  h1 {
    color: ${(props) => props.theme.colors.colorWhite};
    font-family: ${(props) => props.theme.text.textBlack};
    font-size: ${(props) => props.theme.text.textFive};
    margin: 0;

    @media (min-width: 768px) {
      font-size: ${(props) => props.theme.text.textEight};
    }
  }

  p {
    font-size: ${(props) => props.theme.text.textThree};
    color: ${(props) => props.theme.colors.colorWhite};

    sup {
      font-size: 50%;
      top: -0.75rem;
    }
    @media (min-width: 768px) {
      font-size: ${pxToRem(24)};
    }
  }

  a {
    font-family: ${(props) => props.theme.text.textBlack};
    color: ${(props) => props.theme.colors.colorWhite};

    &.button {
      color: ${(props) => props.theme.colors.colorWhite};
    }
  }
`;

const Page404HeroWrapperContentCtas = styled.div`
  a,
  button {
    margin: 0 0 ${(props) => props.theme.spacing.spacingTwo};
    font-family: ${(props) => props.theme.text.textBlack};
    color: ${(props) => props.theme.colors.colorWhite};
    @media (min-width: 768px) {
      margin: 0 ${(props) => props.theme.spacing.spacingTwo};
    }
    &.button {
      color: ${(props) => props.theme.colors.colorWhite};
    }

    &:last-child {
      margin: 0;
      @media (min-width: 768px) {
        margin: 0 ${(props) => props.theme.spacing.spacingTwo};
      }
    }
  }
`;

const Page404ContentWrapper = styled.div`
  padding: ${(props) => props.theme.spacing.spacingFour} 0;

  @media (min-width: 768px) {
    padding: ${(props) => props.theme.spacing.spacingEight} 0;
  }
`;

const Page404ContentCardContent = styled.div`
  padding: ${(props) => props.theme.spacing.spacingThree};

  figure {
    margin: -${(props) => props.theme.spacing.spacingThree} -${(props) =>
        // eslint-disable-next-line prettier/prettier
        props.theme.spacing.spacingThree} ${(props) => props.theme.spacing.spacingTwo};
    img {
      border-top-left-radius: ${pxToRem(12)};
      border-top-right-radius: ${pxToRem(12)};
    }
  }

  h2,
  h3 {
    font-family: ${(props) => props.theme.text.textBlack};
  }

  object {
    margin: ${(props) => props.theme.spacing.spacingThree}
      ${(props) => props.theme.spacing.spacingThree} 0;
  }
`;

const NotFoundPage = () => {
  return typeof window !== 'undefined' && window ? (
    <>
      {(window as any).internalRedirectTo ? (
        <Loading redirectLink={(window as any).internalRedirectTo} />
      ) : (
        <Layout>
          <SEO
            title="404: Not found"
            bodyAttr={{
              id: `404`,
              class: `page-404`,
            }}
            meta={[
              {
                property: 'og:image',
                content: defatulSocialImage,
              },
            ]}
          />
          <Page404HeroWrapper>
            <div className="container">
              <Page404HeroWrapperContent>
                <h1>Page not found.</h1>
                <p>
                  Sorry, but the page you were trying to view does not exist.
                </p>
              </Page404HeroWrapperContent>
              <Page404HeroWrapperContentCtas>
                <a
                  data-button="back to our homepage"
                  className="button button--md button--height--md button--primary"
                  aria-label="Back to our homepage"
                  href={`${process.env.GATSBY_SITE_URL}`}>
                  Back to our homepage
                </a>
              </Page404HeroWrapperContentCtas>
            </div>
          </Page404HeroWrapper>
          <Page404ContentWrapper>
            <div className="container">
              <Flex flexWrap="wrap" justifyContent="center">
                <Box width={[1, 1, 1, 1 / 3]} p={3}>
                  <Card>
                    <Page404ContentCardContent>
                      <figure>
                        <img
                          src="https://cdn.sanity.io/images/86mn51w1/staging/cf0660fe7ea7aed59c962f8bd1343bfd0919d17f-1200x630.jpg?w=780"
                          alt="Newsroom"
                        />
                      </figure>
                      <h3>Newsroom</h3>
                      <p>
                        This uno Newsroom is your one stop shop for all your
                        home loan news. As Australia’s first Active Home Loan
                        Manager, staying on top of all things home loans is just
                        what we do – but we also believe it is important to help
                        you make better decisions day-to-day around anything
                        home loan related.
                      </p>
                      <a
                        href={`${process.env.GATSBY_SITE_URL}/categories/newsroom`}
                        className="button button--fullwidth button--height--md button--primary">
                        Visit our Newsroom
                      </a>
                    </Page404ContentCardContent>
                  </Card>
                </Box>
                <Box width={[1, 1, 1, 1 / 3]} p={3}>
                  <Card>
                    <Page404ContentCardContent>
                      <figure>
                        <img
                          src="https://cdn.sanity.io/images/86mn51w1/staging/cabe59d5655951d4a4cfd55c1bfc0c3cf6178e69-1200x630.jpg?w=780"
                          alt="loanScore"
                        />
                      </figure>
                      <h3>Check your loanScore</h3>
                      <p>
                        Think of loanScore as a free check-up for your home
                        loan. It helps you understand whether your home loan is
                        in good shape2. The lower your score, the higher the
                        chance that you could save! Getting your loanScore is
                        super easy and only takes 2 minutes.
                      </p>
                      <a
                        href={`${process.env.GATSBY_SITE_URL}/analyse-my-loan`}
                        className="button button--fullwidth button--height--md button--primary"
                        aria-label="Get your loanScore">
                        Get your loanScore
                      </a>
                    </Page404ContentCardContent>
                  </Card>
                </Box>
                <Box width={[1, 1, 1, 1 / 3]} p={3}>
                  <Card>
                    <Page404ContentCardContent>
                      <figure>
                        <img
                          src="https://cdn.sanity.io/images/86mn51w1/staging/64f24cae4e31fd5e7e142ca5a06dcff609d66b7c-1200x630.jpg?w=780"
                          alt="Newsroom"
                        />
                      </figure>
                      <h3>Best home loan rates</h3>
                      <p>
                        Compare some of the best home loan rates from a wide
                        range of Australian lenders. Our home loan rates are
                        updated every day. Get expert advice through uno and
                        enquire to see whether we can negotiate an even better
                        rate based on your situation.
                      </p>
                      <a
                        href={`${process.env.GATSBY_SITE_URL}/best-home-loan-rates/`}
                        className="button button--fullwidth button--height--md button--primary"
                        aria-label="See our best home loan rates">
                        See our best home loan rates
                      </a>
                    </Page404ContentCardContent>
                  </Card>
                </Box>
              </Flex>
            </div>
          </Page404ContentWrapper>
        </Layout>
      )}
    </>
  ) : null;
};

export default NotFoundPage;
