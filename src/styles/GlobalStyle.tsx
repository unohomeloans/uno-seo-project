import { createGlobalStyle } from 'styled-components';
import theme from './theme';

const GlobalStyle = createGlobalStyle`
    html {
        font-family: sans-serif;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
    }
    @media only screen and (max-width: 480px) {
        html {
            font-size: 100%;
        }
    }
    article,
    aside,
    details,
    figcaption,
    figure,
    footer,
    /* header, */
    main,
    menu,
    nav,
    section,
    summary {
        display: block;
    }
    audio,
    canvas,
    progress,
    video {
        display: inline-block;
    }
    audio:not([controls]) {
        display: none;
        height: 0;
    }
    progress {
        vertical-align: baseline;
    }
    [hidden],
    template {
        display: none;
    }

    html {
        font-family: sans-serif;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
    }
    body {
        color: ${theme.colors.colorBlack};
        font-family: ${theme.text.textBook};
        font-size: ${theme.text.textThreee};
        margin: 0;
        font-weight: normal;
        word-wrap: break-word;
        font-kerning: normal;
        -moz-font-feature-settings: "kern", "liga", "clig", "calt";
        -ms-font-feature-settings: "kern", "liga", "clig", "calt";
        -webkit-font-feature-settings: "kern", "liga", "clig", "calt";
        font-feature-settings: "kern", "liga", "clig", "calt";
        text-rendering: optimizeLegibility;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }
    article,
    aside,
    details,
    figcaption,
    figure,
    footer,
    /* header, */
    main,
    menu,
    nav,
    section,
    summary {
        display: block;
    }
    audio,
    canvas,
    progress,
    video {
        display: inline-block;
    }
    audio:not([controls]) {
        display: none;
        height: 0;
    }
    progress {
        vertical-align: baseline;
    }
    [hidden],
    template {
        display: none;
    }
    a {
        color: ${theme.colors.colorPrimary};
        background-color: transparent;
        text-decoration: none;
        -webkit-text-decoration-skip: objects;
    }
    p {
        color: ${theme.colors.colorBlack};
    }
    a:active,
    a:hover {
        outline-width: 0;
    }
    abbr[title] {
        border-bottom: none;
        text-decoration: underline;
        text-decoration: underline dotted;
    }
    b,
    strong {
        font-weight: inherit;
        font-weight: bolder;
    }
    dfn {
        font-style: italic;
    }
    h1 {
        font-size: 2em;
        margin: 0.67em 0;
    }
    mark {
        background-color: #ff0;
        color: #000;
    }
    small {
        font-size: .75rem;
        color: #7d8d99;
        line-height: 1.25rem;
    }
    sub,
    sup {
        font-size: 75%;
        line-height: 0;
        position: relative;
        vertical-align: baseline;
    }
    sub {
        bottom: -0.25em;
    }
    sup {
        top: -0.5em;
    }
    img {
        border-style: none;
    }
    svg:not(:root) {
        overflow: hidden;
    }
    code,
    kbd,
    pre,
    samp {
        font-family: monospace, monospace;
        font-size: 1em;
    }
    figure {
        margin: 1em 40px;
    }
    hr {
        box-sizing: content-box;
        height: 0;
        overflow: visible;
    }
    button {
     cursor: pointer;
    }
    button,
    input,
    optgroup,
    select,
    textarea {
        font: inherit;
        margin: 0;
    }
    optgroup {
        font-weight: 700;
    }
    button,
    input {
        overflow: visible;
    }
    button,
    select {
        text-transform: none;
    }
    [type="reset"],
    [type="submit"],
    button,
    html [type="button"] {
        -webkit-appearance: button;
    }
    [type="button"]::-moz-focus-inner,
    [type="reset"]::-moz-focus-inner,
    [type="submit"]::-moz-focus-inner,
    button::-moz-focus-inner {
        border-style: none;
        padding: 0;
    }
    [type="button"]:-moz-focusring,
    [type="reset"]:-moz-focusring,
    [type="submit"]:-moz-focusring,
        button:-moz-focusring {
        outline: 1px dotted ButtonText;
    }
    fieldset {
        border: 1px solid silver;
        margin: 0 2px;
        padding: 0.35em 0.625em 0.75em;
    }
    legend {
        box-sizing: border-box;
        color: inherit;
        display: table;
        max-width: 100%;
        padding: 0;
        white-space: normal;
    }
    textarea {
        overflow: auto;
    }
    [type="checkbox"],
    [type="radio"] {
        box-sizing: border-box;
        padding: 0;
    }
    [type="number"]::-webkit-inner-spin-button,
    [type="number"]::-webkit-outer-spin-button {
        height: auto;
    }
    [type="search"] {
        -webkit-appearance: textfield;
        outline-offset: -2px;
    }
    [type="search"]::-webkit-search-cancel-button,
    [type="search"]::-webkit-search-decoration {
        -webkit-appearance: none;
    }
    ::-webkit-input-placeholder {
        color: inherit;
        opacity: 0.54;
    }
    ::-webkit-file-upload-button {
        -webkit-appearance: button;
        font: inherit;
    }
    html {
        box-sizing: border-box;
        overflow-y: scroll;
    }
    * {
        box-sizing: inherit;
    }
    *:before {
        box-sizing: inherit;
    }
    *:after {
        box-sizing: inherit;
    }
    blockquote {
        margin: ${theme.spacing.spacingThree} 0;
        padding: ${theme.spacing.spacingTwo} 0 ${theme.spacing.spacingTwo} ${theme.spacing.spacingTwo};
        font-size: ${theme.text.textFive};
        font-family: ${theme.text.textMedium};
        color: ${theme.colors.colorBlack};
        position: relative;

    @media (min-width: 768px) {
        font-size: ${theme.text.textSeven};
        padding: ${theme.spacing.spacingFour} 0 ${theme.spacing.spacingFour} ${theme.spacing.spacingFour};
        margin: ${theme.spacing.spacingFour} 0;
    }
        &:before {
            content: "";
            display: inline-block;
            background: ${theme.colors.colorPrimary};
            position: absolute;
            top: 0;
            left: 0;
            width: 3px;
            height: 100%;
        }
    }
    ul {
        margin-left: 1.3rem;
        margin-right: 0;
        margin-top: 0;
        padding-bottom: 0;
        padding-left: 0;
        padding-right: 0;
        padding-top: 0;
        margin-bottom: 1.3rem;
        list-style-position: outside;
        list-style-image: none;
    }
    ol {
        margin-left: 1.3rem;
        margin-right: 0;
        margin-top: 0;
        padding-bottom: 0;
        padding-left: 0;
        padding-right: 0;
        padding-top: 0;
        margin-bottom: 1.3rem;
        list-style-position: outside;
        list-style-image: none;
    }
    dl {
        margin-left: 0;
        margin-right: 0;
        margin-top: 0;
        padding-bottom: 0;
        padding-left: 0;
        padding-right: 0;
        padding-top: 0;
        margin-bottom: 1.3rem;
    }
    dd {
        margin-left: 0;
        margin-right: 0;
        margin-top: 0;
        padding-bottom: 0;
        padding-left: 0;
        padding-right: 0;
        padding-top: 0;
        margin-bottom: 1.3rem;
    }
    li {
        margin-bottom: calc(1.3rem / 2);
        font-size: 1.25rem;
    }
    ol li {
        padding-left: 0;
        font-size: 1.25rem;
    }
    ul li {
        padding-left: 0;
    }
    li > ol {
        margin-left: 1.3rem;
        margin-bottom: calc(1.3rem / 2);
        margin-top: calc(1.3rem / 2);
    }
    li > ul {
        margin-left: 1.3rem;
        margin-bottom: calc(1.3rem / 2);
        margin-top: calc(1.3rem / 2);
    }
    blockquote *:last-child {
        margin-bottom: 0;
    }
    li *:last-child {
        margin-bottom: 0;
    }
    p *:last-child {
        margin-bottom: 0;
    }
    li > p {
        margin-bottom: calc(1.3rem / 2);
    }
    h1 {
        margin: 0 0 1.45rem 0;
        padding: 0;
        color: ${theme.colors.colorBlack};
        font-family: ${theme.text.textMedium};
        font-weight: 400;
        text-rendering: optimizeLegibility;
        line-height: 1.1;

        @media (max-width: 768px) {
            font-size: ${theme.text.textSix};
        }
    }
    h2 {
        margin: 0 0 1.45rem 0;
        padding: 0;
        color: ${theme.colors.colorBlack};
        font-family: ${theme.text.textMedium};
        font-weight: 400;
        text-rendering: optimizeLegibility;
        line-height: 1.1;

        @media (max-width: 768px) {
            font-size: ${theme.text.textFive};
        }
    }

    h4 {
        font-family: ${theme.text.textBlack};
    }

    h5 {
        font-family: ${theme.text.textBlack};
    }

    .no-scroll {
        overflow: hidden;
    }
`;

export default GlobalStyle;
