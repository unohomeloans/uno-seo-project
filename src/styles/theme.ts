import pxToRem from '../utils/pxToRem';

export default {
  colors: {
    colorPrimary: '#337772',
    colorBlack: '#284053',
    colorSecondary: '#2C2F55',
    colorAlternate: '2A2E59',
    colorWhite: '#fff',
    colorPrimaryGradient: 'linear-gradient (135deg, #2C2F55 0%, #337772 100%)',
    colorLoanScoreRed: 'linear-gradient (135deg, #FF4551 0%, #D21925 100%)',
    colorLoanScoreAmber: 'linear-gradient (135deg, #FFA50F 0%, #FF6B00 100%)',
    colorLoanScoreGreen: 'linear-gradient (135deg, #00BA65 0%, #04A158 100%)',
    colorAccent: '#F7F7F5',
    colorGrey01: '#FBFBFC',
    colorGrey02: '#D4D9DD',
    colorGrey03: '#5E707E',
    colorGrayDark: '#526675',
    colorGrayDarkSecondary: '#7e8d98',
    colorGrayLight: '#e9ebed',
    colorSuccess: '#04A158',
    colorWarning: '#FF6B00',
    colorDanger: '#D21925',
    colorInfo: '#2577D7',
    colorYellow: '#FFE24F',
  },
  text: {
    textBlack: 'Avenir-Black, sans-serif',
    textHeavy: 'Avenir-Heavy, sans-serif',
    textBook: 'Avenir-Book, sans-serif',
    textMedium: 'Avenir-Medium, sans-serif',
    fontSizeSmaller: '1.125rem',
    fontSizeMedium: '1.5rem',
    fontMedium: 'Avenir-Medium',
    textOne: pxToRem(12),
    textTwo: pxToRem(14),
    textThreee: pxToRem(16),
    textFour: pxToRem(18),
    textFive: pxToRem(20),
    textSix: pxToRem(28),
    textSeven: pxToRem(32),
    textEight: pxToRem(40),
    textNine: pxToRem(48),
  },
  spacing: {
    spacingXXSmall: '.5rem',
    spacingXSmall: '1rem',
    spacingLarge: '2.5rem',
    spacingOne: pxToRem(8),
    spacingTwo: pxToRem(16),
    spacingThree: pxToRem(24),
    spacingFour: pxToRem(32),
    spacingFive: pxToRem(40),
    spacingSix: pxToRem(48),
    spacingSeven: pxToRem(56),
    spacingEight: pxToRem(64),
  },
};
