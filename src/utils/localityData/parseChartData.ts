import dayjs from 'dayjs';

export default (
  dateArray: string[],
  valueArray: number[]
): {
  date: string;
  value: number;
}[] =>
  dateArray.map((date, i) => ({
    date: dayjs(date).format('MMM YY'),
    value: valueArray[i],
  }));
