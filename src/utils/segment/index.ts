import engaged from './engaged';
import page from './page';
import scroll from './scroll';
import track from './track';

const SEGMENT_EVENT_TYPE_ENGAGED = 'engaged';
const SEGMENT_EVENT_TYPE_SCROLL = 'scroll';
const SEGMENT_EVENT_TYPE_PAGE = 'page';
const SEGMENT_EVENT_TYPE_TRACK = 'track';
const SEGMENT_EVENT_TYPE_VALUES = [
  SEGMENT_EVENT_TYPE_ENGAGED,
  SEGMENT_EVENT_TYPE_PAGE,
  SEGMENT_EVENT_TYPE_SCROLL,
  SEGMENT_EVENT_TYPE_TRACK,
] as const;

const SEGMENT_EVENT_FUNCTIONS = {
  [SEGMENT_EVENT_TYPE_ENGAGED]: engaged,
  [SEGMENT_EVENT_TYPE_SCROLL]: scroll,
  [SEGMENT_EVENT_TYPE_PAGE]: page,
  [SEGMENT_EVENT_TYPE_TRACK]: track,
};

type SegmentEventTypes = typeof SEGMENT_EVENT_TYPE_VALUES[number];

export default (eventType: SegmentEventTypes, properties?: any): void => {
  const segmentJS = (window as any).analytics;

  const isSegmentAvailable =
    !segmentJS || segmentJS !== undefined || segmentJS !== null;

  if (!isSegmentAvailable) {
    console.log('Segment is not available');
    return;
  }

  SEGMENT_EVENT_FUNCTIONS[eventType](properties);
};
