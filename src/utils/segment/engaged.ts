export default (): void =>
  (window as any).analytics.track('Engaged', {
    time: 29,
    origin: window.location.href,
    eventSource: 'clientSide',
    codeSource: 'gatsby',
  });
