export default (properties?: any): void => {
  (window as any).analytics.page(document.title, {
    properties: {
      ...properties,
      path: location.pathname,
      referrer: document.referrer,
      eventSource: 'clientSide',
      codeSource: 'gatsby',
    },
  });
};
