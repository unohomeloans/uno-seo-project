export default (px: number): string => `${px / 16}rem`;
