export default (accentedWord: string): string =>
  accentedWord.replace(/\s/g, '');
