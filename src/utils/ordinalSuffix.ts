export default (n: number): string => {
  const suffix = ['st', 'nd', 'rd'][((((n + 90) % 100) - 10) % 10) - 1] || 'th';
  return `${n}${suffix}`;
};
