import React, { useEffect } from 'react';
import { ThemeProvider } from 'styled-components';
import isNil from 'lodash/isNil';
import queryString from 'querystring';
import { Helmet } from 'react-helmet';
import { COOKIE_EXPIRES, UNO_REFERRER, setCookie } from '../../utils/cookies';
import useRouter from '../../hooks/useRouter';
import theme from '../../styles/theme';
import GlobalStyle from '../../styles/GlobalStyle';
import Header from '../Header';
import Footer from '../Footer';
import { StyledMain } from './Layout.styled';

import '../../styles/scss/main.scss';

const Layout: React.FC = ({ children }) => {
  const router = useRouter();
  useEffect(() => {
    let utmSource = '';
    let utmMedium = '';
    let utmTerm = '';
    let utmContent = '';
    let utmCampaign = '';
    let unoReferrer = '';

    const parsedSearch = queryString.parse(router.search);

    if (isNil(parsedSearch)) {
      return;
    }

    if (!isNil(parsedSearch['utm_source'])) {
      utmSource = parsedSearch['utm_source'] as string;
      setCookie('utm_source', utmSource, { expires: COOKIE_EXPIRES });
    }

    if (!isNil(parsedSearch['utm_medium'])) {
      utmMedium = parsedSearch['utm_medium'] as string;
      setCookie('utm_medium', utmMedium, { expires: COOKIE_EXPIRES });
    }
    if (!isNil(parsedSearch['utm_term'])) {
      utmTerm = parsedSearch['utm_term'] as string;
      setCookie('utm_term', utmTerm, { expires: COOKIE_EXPIRES });
    }
    if (!isNil(parsedSearch['utm_content'])) {
      utmContent = parsedSearch['utm_content'] as string;
      setCookie('utm_content', utmContent, { expires: COOKIE_EXPIRES });
    }

    if (!isNil(parsedSearch['utm_campaign'])) {
      utmCampaign = parsedSearch['utm_campaign'] as string;
      setCookie('utm_campaign', utmCampaign, { expires: COOKIE_EXPIRES });
    }
    if (!isNil(parsedSearch['uno-referrer'])) {
      unoReferrer = parsedSearch['uno-referrer'] as string;
      setCookie(UNO_REFERRER, unoReferrer, { expires: COOKIE_EXPIRES });
    }
  }, [router.search]);

  return (
    <>
      <Helmet>
        <link
          href="https://assets.calendly.com/assets/external/widget.css"
          rel="stylesheet"
        />
        <script
          src="https://assets.calendly.com/assets/external/widget.js"
          type="text/javascript"
          async></script>
      </Helmet>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Header />
        <StyledMain>{children}</StyledMain>
        <Footer />
      </ThemeProvider>
    </>
  );
};

export default Layout;
