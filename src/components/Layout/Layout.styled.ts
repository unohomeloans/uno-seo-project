import styled from 'styled-components';
import pxToRem from '../../utils/pxToRem';

export const StyledMain = styled.main`
  padding: ${pxToRem(90)} 0 0;

  @media (min-width: 992px) {
    padding: 0;
  }
`;
