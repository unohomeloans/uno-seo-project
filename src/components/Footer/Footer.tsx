import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import isNil from 'lodash/isNil';
import useSiteMetadata from '../../hooks/useSiteMetadata';
import {
  FooterContentWrappper,
  FooterContentItem,
  FooterGridContainer,
  FooterGridContainerItem,
  FooterGridContainerLabel,
  FooterGridContainerList,
  FooterGridContainerListItem,
  FooterGridContainerLink,
} from './Footer.styled';

const unoLogoWhite =
  'https://cdn.unohomeloans.com.au/images/uno-logo-white.svg';
const icoSecurity =
  'https://cdn.unohomeloans.com.au/images/icon-secure-white.svg';

const FooterItems = () => (
  <StaticQuery
    query={graphql`
      query Items {
        items: allSanityMenus(
          filter: { slug: { current: { eq: "footer-nav" } } }
        ) {
          nodes {
            title
            id
            _id
            items {
              navItemLabel
              navItemType
              ctas {
                title
                linkedPage {
                  ... on SanityPage {
                    id
                    content {
                      main {
                        title
                        slug {
                          current
                        }
                      }
                    }
                    __typename
                  }
                  ... on SanityPost {
                    id
                    title
                    slug {
                      _key
                      _type
                      current
                    }
                    __typename
                  }
                  ... on SanityCategory {
                    id
                    title
                    slug {
                      _key
                      _type
                      current
                    }
                    __typename
                  }
                }
                link {
                  title
                  link
                }
                applink {
                  title
                  select
                  append
                }
              }
            }
          }
        }
      }
    `}
    render={(data) => {
      return (
        <>
          {data.items.nodes.map((item: any, index: any) => {
            if (item.title === 'Footer Nav') {
              return (
                <FooterGridContainer key={index}>
                  {item.items.map((navItem: any, index: any) => {
                    return (
                      <FooterGridContainerItem key={index}>
                        <FooterGridContainerLabel
                          data-item={navItem.navItemLabel}>
                          {navItem.navItemLabel}
                        </FooterGridContainerLabel>
                        <FooterGridContainerList>
                          {navItem.ctas.map((ctaItem: any, i: any) => {
                            const LS = process.env.GATSBY_MY_SANITY_LS;
                            const CV2 = process.env.GATSBY_MY_SANITY_CV2;

                            const checkTypeNamePage =
                              !isNil(ctaItem.linkedPage) &&
                              isNil(ctaItem.link) &&
                              ctaItem.linkedPage.__typename === 'SanityPage';
                            const checkTypeNamePost =
                              !isNil(ctaItem.linkedPage) &&
                              isNil(ctaItem.link) &&
                              ctaItem.linkedPage.__typename === 'SanityPost';
                            const checkTypeNameCategory =
                              !isNil(ctaItem.linkedPage) &&
                              isNil(ctaItem.link) &&
                              ctaItem.linkedPage.__typename ===
                                'SanityCategory';
                            const checkTypeNameExternal = !isNil(ctaItem.link);
                            const checkTypeNameApplink = !isNil(
                              ctaItem.applink
                            );

                            if (checkTypeNamePage) {
                              return (
                                <FooterGridContainerListItem key={i}>
                                  <FooterGridContainerLink
                                    key={i}
                                    data-url={
                                      ctaItem.linkedPage.content.main.slug
                                        .current
                                    }
                                    href={`https://unohomeloans.com.au/${ctaItem.linkedPage.content.main.slug.current}/`}>
                                    {ctaItem.linkedPage.content.main.title}
                                  </FooterGridContainerLink>
                                </FooterGridContainerListItem>
                              );
                            }
                            if (checkTypeNamePost) {
                              return (
                                <FooterGridContainerListItem key={i}>
                                  <FooterGridContainerLink
                                    key={i}
                                    data-url={ctaItem.linkedPage.slug.current}
                                    href={`https://unohomeloans.com.au/${ctaItem.linkedPage.slug.current}/`}>
                                    {ctaItem.linkedPage.title}
                                  </FooterGridContainerLink>
                                </FooterGridContainerListItem>
                              );
                            }
                            if (checkTypeNameCategory) {
                              return (
                                <FooterGridContainerListItem key={i}>
                                  <FooterGridContainerLink
                                    key={i}
                                    data-url={ctaItem.linkedPage.slug.current}
                                    href={`https://unohomeloans.com.au/categories/${ctaItem.linkedPage.slug.current}`}>
                                    {ctaItem.title}
                                  </FooterGridContainerLink>
                                </FooterGridContainerListItem>
                              );
                            }
                            if (checkTypeNameExternal) {
                              return (
                                <FooterGridContainerListItem key={i}>
                                  <FooterGridContainerLink
                                    key={i}
                                    data-url={ctaItem.link.link}
                                    href={`https://unohomeloans.com.au${ctaItem.link.link}/`}>
                                    {ctaItem.link.title}
                                  </FooterGridContainerLink>
                                </FooterGridContainerListItem>
                              );
                            }
                            if (
                              checkTypeNameApplink &&
                              ctaItem.applink.select === 'CV2'
                            ) {
                              const withAppendCV2 = !isNil(
                                ctaItem.applink.append
                              )
                                ? ctaItem.applink.append
                                : '';

                              return (
                                <FooterGridContainerListItem key={i}>
                                  <FooterGridContainerLink
                                    key={i}
                                    data-url={`${CV2}${withAppendCV2}`}
                                    href={`${CV2}${withAppendCV2}`}>
                                    {ctaItem.applink.title}
                                  </FooterGridContainerLink>
                                </FooterGridContainerListItem>
                              );
                            }
                            if (
                              checkTypeNameApplink &&
                              ctaItem.applink.select === 'LS'
                            ) {
                              const withAppendLS = !isNil(
                                ctaItem.applink.append
                              )
                                ? ctaItem.applink.append
                                : '';

                              return (
                                <FooterGridContainerListItem key={i}>
                                  <FooterGridContainerLink
                                    key={i}
                                    data-url={`${LS}${withAppendLS}`}
                                    href={`${LS}${withAppendLS}`}>
                                    {ctaItem.applink.title}
                                  </FooterGridContainerLink>
                                </FooterGridContainerListItem>
                              );
                            }

                            return null;
                          })}
                        </FooterGridContainerList>
                      </FooterGridContainerItem>
                    );
                  })}
                </FooterGridContainer>
              );
            }
            return null;
          })}
        </>
      );
    }}
  />
);

const Footer: React.FC = () => {
  const { footerCopyright, socialLinks } = useSiteMetadata();

  return (
    <>
      <div className="footer">
        <div className="container">
          <div className="footer--content">
            <FooterContentWrappper>
              <FooterContentItem className="footer--content-item">
                <img
                  alt="Uno Logo"
                  src={unoLogoWhite}
                  data-logo={unoLogoWhite}
                />
                <div className="footer--content-item--social">
                  {socialLinks.map((item: any, index: any) => (
                    <a
                      data-url={item.title}
                      key={index}
                      href={item.link}
                      title={item.title}
                      rel="_blank">
                      <img alt={item.title} src={item.icon} />
                    </a>
                  ))}
                </div>
                <div className="footer--content-item--privacy">
                  <div className="footer--content-item--privacy-logo">
                    <img
                      src={icoSecurity}
                      alt="Uno Security"
                      data-image={icoSecurity}
                    />
                  </div>
                  <div
                    className="footer--content-item--privacy-content"
                    data-content="Your information is protected by bank-level security covered by an industry-first insurance policy.">
                    Your information is protected by bank-level security covered
                    by an industry-first insurance policy.
                  </div>
                </div>
              </FooterContentItem>
              <FooterContentItem className="footer--content-item">
                <FooterItems />
              </FooterContentItem>
            </FooterContentWrappper>
          </div>
        </div>
        <div className="container">
          <div className="footer-copyright" data-footer={footerCopyright}>
            © {new Date().getFullYear()} {footerCopyright}
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
