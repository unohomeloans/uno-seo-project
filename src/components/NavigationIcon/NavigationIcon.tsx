import React from 'react';

const NavigationIcon: React.FC = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="18"
    height="12"
    viewBox="0 0 18 12">
    <g fill="none" fillRule="evenodd" stroke="none" strokeWidth="1">
      <g fill="#337772" transform="translate(-19 -24)">
        <g transform="translate(16 16)">
          <g transform="translate(0 2)">
            <path d="M19.941 16c.585 0 1.059.448 1.059 1s-.474 1-1.059 1H4.06C3.474 18 3 17.552 3 17s.474-1 1.059-1H19.94zm0-5c.585 0 1.059.448 1.059 1s-.474 1-1.059 1H4.06C3.474 13 3 12.552 3 12s.474-1 1.059-1H19.94zm0-5C20.526 6 21 6.448 21 7s-.474 1-1.059 1H4.06C3.474 8 3 7.552 3 7s.474-1 1.059-1H19.94z"></path>
          </g>
        </g>
      </g>
    </g>
  </svg>
);

export default NavigationIcon;
