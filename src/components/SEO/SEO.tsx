import React from 'react';
import Helmet from 'react-helmet';
import { StaticQuery, graphql } from 'gatsby';

const detailsQuery = graphql`
  query DefaultSEOQuery {
    site: sanitySiteSettings(_id: { regex: "/(drafts.|)siteSettings/" }) {
      title
      description
    }
    post: sanityPost {
      seoDescription
      seoTitle
    }
    page: sanityPage {
      content {
        main {
          title
        }
      }
    }
  }
`;

interface SEOProps {
  title?: string;
  description?: string;
  lang?: string;
  meta?: any[];
  keywords?: string[];
  bodyAttr?: any;
}

const SEO: React.FC<SEOProps> = ({
  description,
  title,
  lang,
  meta,
  keywords,
  bodyAttr,
}) => {
  return (
    <StaticQuery
      query={detailsQuery}
      render={(data) => {
        const metaDescription =
          description || (data.site && data.site.description) || '';
        const siteTitle = (data.title && data.site.title) || 'uno';
        const siteAuthor =
          (data.site && data.site.author && data.site.author.name) || '';
        const pageTitle = title || siteTitle;

        return (
          <Helmet
            bodyAttributes={bodyAttr}
            htmlAttributes={{ lang: lang || 'en' }}
            title={pageTitle}
            titleTemplate={
              pageTitle === siteTitle ? siteTitle : `%s | ${siteTitle}`
            }
            meta={[
              {
                name: 'google-site-verification',
                content: '',
              },
              {
                name: 'description',
                content: metaDescription,
              },
              {
                property: 'og:title',
                content: title,
              },
              {
                property: 'og:description',
                content: metaDescription,
              },
              {
                property: 'og:type',
                content: 'website',
              },
              {
                name: 'twitter:card',
                content: 'summary',
              },
              {
                name: 'twitter:creator',
                content: siteAuthor,
              },
              {
                name: 'twitter:title',
                content: title,
              },
              {
                name: 'twitter:description',
                content: metaDescription,
              },
            ]
              .concat(
                keywords && keywords.length > 0
                  ? {
                      name: 'keywords',
                      content: keywords.join(', '),
                    }
                  : []
              )
              .concat(meta ? meta : [])}>
            <script
              src={`https://cdn.optimizely.com/js/${process.env.GATSBY_MY_OPTIMIZELY}.js`}></script>
          </Helmet>
        );
      }}
    />
  );
};

export default SEO;
