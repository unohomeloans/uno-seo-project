import React, { useEffect, useRef, useState } from 'react';
import { StyledHeaderContainer } from './Header.styled';
import CTAs from '../CTAs';
import Menu from '../Menu';
import HeaderLogo from '../HeaderLogo';
import NavigationIcon from '../NavigationIcon';

interface HeaderProps {
  idSelector?: string;
}

const setIsOpenClassName = (isOpen: boolean): string =>
  isOpen ? 'is-open' : '';

const Header: React.FC<HeaderProps> = ({ idSelector }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const idPrefix = idSelector || 'header';

  const navRef = useRef<HTMLElement>(null);

  useEffect(() => {
    const htmlElement = document.querySelector('html');

    if (htmlElement) {
      if (isOpen) {
        htmlElement.classList.add('no-scroll');
      } else {
        htmlElement.classList.remove('no-scroll');
      }
    }
  }, [isOpen]);

  useEffect(() => {
    if (navRef.current) {
      const contactUsHeaderLink = navRef.current.querySelector(
        `li.menu-item a[href*='unohomeloans.com.au/contact']`
      ) as HTMLAnchorElement;

      if (contactUsHeaderLink) {
        contactUsHeaderLink.href = '';
        contactUsHeaderLink.addEventListener('click', (e) => {
          e.stopPropagation();
          e.stopImmediatePropagation();
          e.preventDefault();
          (window as any).Calendly.initPopupWidget({
            url: 'https://calendly.com/uno-customer-care/uno-quick-call',
          });
          return false;
        });
      }
    }
  }, [navRef]);

  const handleNavTriggerClick = () => setIsOpen(!isOpen);

  const handleCloseClick = () => setIsOpen(false);

  return (
    <StyledHeaderContainer id={`${idPrefix}-container`}>
      <header id={idPrefix} className={`header ${setIsOpenClassName(isOpen)}`}>
        <HeaderLogo />
        <div className="nav-trigger-wrapper">
          <button
            className="nav-trigger"
            aria-expanded={isOpen}
            onClick={handleNavTriggerClick}>
            <NavigationIcon />
          </button>
        </div>
        <nav
          id={`${idPrefix}-nav`}
          className={`main-nav ${setIsOpenClassName(isOpen)}`}
          ref={navRef}>
          <Menu idSelector={`${idPrefix}-menu`} />
          <div id={`${idPrefix}-menu-ctas`} className="main-nav__ctas">
            <div
              id={`${idPrefix}-menu-ctas-item`}
              className="menu-nav__ctas--item">
              <a
                id={`${idPrefix}-menu-ctas-item-getStarted`}
                className="button button--primary button--fullwidth"
                href="/find-me-homeloan"
                data-link="Get Started">
                Get Started
              </a>
            </div>
            <div className="main-nav__ctas--item">
              <a
                id={`${idPrefix}-menu-ctas-item-login`}
                href="/home-loans/#sign-in"
                data-link="Login">
                Login
              </a>
            </div>
            <div className="main-nav__ctas--item">
              <div>
                <svg
                  className="nav-header--item"
                  onClick={handleCloseClick}
                  role="button"
                  tabIndex={0}
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  width="20"
                  height="20"
                  viewBox="0 0 20 20">
                  <defs>
                    <path
                      id="path-1"
                      d="M2.77 1.052l7.141 7.142 7.141-7.141A1.214 1.214 0 1118.77 2.77l-7.142 7.142 7.142 7.142a1.214 1.214 0 01-1.718 1.717l-7.14-7.142L2.77 18.77a1.214 1.214 0 01-1.717-1.717l7.141-7.142-7.141-7.143A1.214 1.214 0 012.77 1.052z"></path>
                  </defs>
                  <g
                    fill="none"
                    fillRule="evenodd"
                    stroke="none"
                    strokeWidth="1">
                    <mask fill="#fff">
                      <use xlinkHref="#path-1"></use>
                    </mask>
                    <use fill="#337772" xlinkHref="#path-1"></use>
                  </g>
                </svg>
              </div>
            </div>
          </div>
        </nav>
        <CTAs />
      </header>
    </StyledHeaderContainer>
  );
};

export default Header;
