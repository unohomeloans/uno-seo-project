import styled from 'styled-components';
import pxToRem from '../../utils/pxToRem';

export const StyledHeaderContainer = styled.div`
  @media (min-width: 992px) {
    margin: 0 auto;
    padding-left: ${pxToRem(15)};
    padding-right: ${pxToRem(15)};
    max-width: ${pxToRem(1360)};
  }
`;
