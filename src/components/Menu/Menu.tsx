import React, { useState } from 'react';
import isNil from 'lodash/isNil';
import { useStaticQuery, Link, graphql } from 'gatsby';
import { isMobile } from 'react-device-detect';
import segment from '../../utils/segment';

interface MenuProps {
  idSelector?: string;
}

const Menu: React.FC<MenuProps> = ({ idSelector }) => {
  const idPrefix = idSelector || 'menu';

  const [expand, setExpand] = useState(false);

  const LS = process.env.GATSBY_MY_SANITY_LS;
  const CV2 = process.env.GATSBY_MY_SANITY_CV2;

  const data = useStaticQuery(graphql`
    query HeaderItems {
      items: allSanityMenus(
        filter: { slug: { current: { eq: "main-navigation" } } }
      ) {
        nodes {
          title
          id
          _id
          items {
            navItemLabel
            navItemType
            ctas {
              linkedPage {
                ... on SanityPage {
                  id
                  content {
                    main {
                      title
                      slug {
                        current
                      }
                    }
                  }
                  __typename
                }
                ... on SanityPost {
                  id
                  title
                  slug {
                    _key
                    _type
                    current
                  }
                  __typename
                }
                ... on SanityCategory {
                  id
                  title
                  slug {
                    _key
                    _type
                    current
                  }
                  __typename
                }
              }
              link {
                title
                link
              }
              applink {
                title
                select
                append
              }
              submenuItems {
                linkedPage {
                  ... on SanityPage {
                    id
                    content {
                      main {
                        title
                        slug {
                          current
                        }
                      }
                    }
                    __typename
                  }
                  ... on SanityPost {
                    id
                    title
                    slug {
                      _key
                      _type
                      current
                    }
                    __typename
                  }
                  ... on SanityCategory {
                    id
                    title
                    slug {
                      _key
                      _type
                      current
                    }
                    __typename
                  }
                }
                link {
                  title
                  link
                }
                applink {
                  title
                  select
                  append
                }
                title
              }
            }
          }
        }
      }
    }
  `);

  return (
    <ul id={idPrefix} className="menu">
      {data.items.nodes.map((item: any, i: any) => {
        return (
          <React.Fragment key={i}>
            {item.items.map((navItem: any, i: any) => {
              return (
                <React.Fragment key={i}>
                  {navItem.ctas.map((ctaItem: any, i: any) => {
                    const GetSubmenuItemsArrow = () => (
                      <React.Fragment key={i}>
                        {ctaItem.submenuItems.length > 0 && (
                          <span
                            key={i + 2}
                            className="menu-item--has-item--icon">
                            <svg
                              width="8"
                              height="5"
                              xmlns="http://www.w3.org/2000/svg"
                              xmlnsXlink="http://www.w3.org/1999/xlink">
                              <defs>
                                <path
                                  d="M5.24 2.5L1.67-.624a.5.5 0 11.66-.752l4 3.5a.5.5 0 010 .753l-4 3.5a.5.5 0 11-.66-.753L5.24 2.5z"
                                  id="a"
                                />
                              </defs>
                              <use
                                fill="#A9B3BA"
                                transform="rotate(90 4 2.5)"
                                xlinkHref="#a"
                                fillRule="evenodd"
                              />
                            </svg>
                          </span>
                        )}
                      </React.Fragment>
                    );

                    const GetSubmenuItems = () => (
                      <>
                        {ctaItem.submenuItems.length > 0 && (
                          <ul className="menu-item--sub-item">
                            {ctaItem.submenuItems.map(
                              (submenuItem: any, i: any) => {
                                const checkSubmenuTypePage =
                                  !isNil(submenuItem.linkedPage) &&
                                  isNil(submenuItem.link) &&
                                  submenuItem.linkedPage.__typename ===
                                    'SanityPage';
                                const checkSubmenuTypePost =
                                  !isNil(submenuItem.linkedPage) &&
                                  isNil(submenuItem.link) &&
                                  submenuItem.linkedPage.__typename ===
                                    'SanityPost';
                                const checkSubmenuTypeCategory =
                                  !isNil(submenuItem.linkedPage) &&
                                  isNil(submenuItem.link) &&
                                  submenuItem.linkedPage.__typename ===
                                    'SanityCategory';
                                const checkSubmenuTypeExternal = !isNil(
                                  submenuItem.link
                                );
                                const checkSubmenuTypeApplink = !isNil(
                                  submenuItem.applink
                                );

                                if (checkSubmenuTypePage) {
                                  const handleSubmenuPagePostClickTracking =
                                    () => {
                                      segment('track', {
                                        trackName: 'click',
                                        text: submenuItem.linkedPage.content
                                          .main.title,
                                        elementType: 'link',
                                        elementID: `link-${submenuItem.linkedPage.content.main.slug.current}`,
                                        colour: 'primary',
                                        destination: `${window.location.origin}/${submenuItem.linkedPage.content.main.slug.current}`,
                                      });
                                    };

                                  return (
                                    <li
                                      key={i}
                                      data-item={
                                        submenuItem.linkedPage.content.main
                                          .title
                                      }
                                      id={`${idPrefix}-item-${i}`}>
                                      <a
                                        data-url={
                                          submenuItem.linkedPage.content.main
                                            .slug.current
                                        }
                                        href={`https://unohomeloans.com.au/${submenuItem.linkedPage.content.main.slug.current}`}
                                        onClick={
                                          handleSubmenuPagePostClickTracking
                                        }>
                                        {submenuItem.title}
                                      </a>
                                    </li>
                                  );
                                }
                                if (checkSubmenuTypePost) {
                                  const handleSubmenuPagePostClickTracking =
                                    () => {
                                      segment('track', {
                                        trackName: 'click',
                                        text: submenuItem.linkedPage.title,
                                        elementType: 'link',
                                        elementID: `link-${submenuItem.linkedPage.slug.current}`,
                                        colour: 'primary',
                                        destination: `${window.location.origin}/${submenuItem.linkedPage.slug.current}/`,
                                      });
                                    };

                                  return (
                                    <li
                                      key={i}
                                      data-item={submenuItem.linkedPage.title}
                                      id={`${idPrefix}-item-${i}`}>
                                      <a
                                        data-url={
                                          submenuItem.linkedPage.slug.current
                                        }
                                        href={
                                          'https://unohomeloans.com.au' +
                                          submenuItem.linkedPage.slug.current
                                        }
                                        onClick={
                                          handleSubmenuPagePostClickTracking
                                        }>
                                        {submenuItem.title}
                                      </a>
                                    </li>
                                  );
                                }
                                if (checkSubmenuTypeCategory) {
                                  const handleSubmenuPageCategoryClickTracking =
                                    () => {
                                      segment('track', {
                                        trackName: 'click',
                                        text: submenuItem.linkedPage.title,
                                        elementType: 'link',
                                        elementID: `link-${submenuItem.linkedPage.slug.current}`,
                                        colour: 'primary',
                                        destination: `${window.location.origin}/categories/${submenuItem.linkedPage.slug.current}/`,
                                      });
                                    };

                                  return (
                                    <li
                                      key={i}
                                      data-item={submenuItem.linkedPage.title}
                                      id={`${idPrefix}-item-${i}`}>
                                      <a
                                        data-url={
                                          submenuItem.linkedPage.slug.current
                                        }
                                        href={`https://unohomeloans.com.au/categories/${submenuItem.linkedPage.slug.current}/`}
                                        onClick={
                                          handleSubmenuPageCategoryClickTracking
                                        }>
                                        {submenuItem.title}
                                      </a>
                                    </li>
                                  );
                                }
                                if (checkSubmenuTypeExternal) {
                                  const handleSubmenuExternalClickTracking =
                                    () => {
                                      segment('track', {
                                        trackName: 'click',
                                        text: submenuItem.link.title,
                                        elementType: 'link',
                                        elementID: `link-${submenuItem.link.link}`,
                                        colour: 'primary',
                                        destination: `${window.location.origin}/${submenuItem.link.link}`,
                                      });
                                    };

                                  return (
                                    <li
                                      key={i}
                                      data-item={submenuItem.link.title}
                                      id={`${idPrefix}-item-${i}`}>
                                      <a
                                        data-url={submenuItem.link.link}
                                        href={`https://unohomeloans.com.au/${submenuItem.link.link}/`}
                                        onClick={
                                          handleSubmenuExternalClickTracking
                                        }>
                                        {submenuItem.link.title}
                                      </a>
                                    </li>
                                  );
                                }
                                if (
                                  checkSubmenuTypeApplink &&
                                  submenuItem.applink.select === 'CV2'
                                ) {
                                  const handleAppClickTracking = () => {
                                    segment('track', {
                                      trackName: 'click',
                                      text: submenuItem.applink.title,
                                      elementType: 'link',
                                      elementID: `link-${CV2}${withAppendCV2}`,
                                      colour: 'primary',
                                      destination: `${CV2}${withAppendCV2}`,
                                    });
                                  };

                                  const withAppendCV2 = !isNil(
                                    submenuItem.applink.append
                                  )
                                    ? submenuItem.applink.append
                                    : '';

                                  return (
                                    <li
                                      key={i}
                                      data-item={submenuItem.applink.title}
                                      id={`${idPrefix}-item-${i}`}>
                                      <a
                                        data-url={`${CV2}${withAppendCV2}`}
                                        href={`${CV2}${withAppendCV2}`}
                                        onClick={handleAppClickTracking}>
                                        {submenuItem.applink.title}
                                      </a>
                                    </li>
                                  );
                                }
                                if (
                                  checkSubmenuTypeApplink &&
                                  submenuItem.applink.select === 'LS'
                                ) {
                                  const handleAppClickTracking = () => {
                                    segment('track', {
                                      trackName: 'click',
                                      text: submenuItem.applink.title,
                                      elementType: 'link',
                                      elementID: `link-${LS}${withAppendLS}`,
                                      colour: 'primary',
                                      destination: `${LS}${withAppendLS}`,
                                    });
                                  };

                                  const withAppendLS = !isNil(
                                    submenuItem.applink.append
                                  )
                                    ? submenuItem.applink.append
                                    : '';

                                  return (
                                    <li
                                      key={i}
                                      data-item={submenuItem.applink.title}
                                      id={`${idPrefix}-item-${i}`}>
                                      <a
                                        data-url={`${LS}${withAppendLS}`}
                                        href={`${LS}${withAppendLS}`}
                                        onClick={handleAppClickTracking}>
                                        {submenuItem.applink.title}
                                      </a>
                                    </li>
                                  );
                                }
                                return null;
                              }
                            )}
                          </ul>
                        )}
                      </>
                    );

                    const checkTypeNamePage =
                      !isNil(ctaItem.linkedPage) &&
                      isNil(ctaItem.link) &&
                      ctaItem.linkedPage.__typename === 'SanityPage';
                    const checkTypeNamePost =
                      !isNil(ctaItem.linkedPage) &&
                      isNil(ctaItem.link) &&
                      ctaItem.linkedPage.__typename === 'SanityPost';
                    const checkTypeNameCategory =
                      !isNil(ctaItem.linkedPage) &&
                      isNil(ctaItem.link) &&
                      ctaItem.linkedPage.__typename === 'SanityCategory';
                    const checkTypeNameExternal = !isNil(ctaItem.link);
                    const checkTypeNameApplink = !isNil(ctaItem.applink);

                    if (checkTypeNamePage) {
                      return (
                        <li
                          key={i}
                          role="presentation"
                          data-item={ctaItem.linkedPage.content.main.title}
                          id={`${idPrefix}-item-${i}`}
                          className={`${
                            ctaItem.submenuItems.length > 0 && expand === true
                              ? 'menu-item menu-item--has-item'
                              : 'menu-item'
                          }`}
                          onClick={() => {
                            setExpand(!expand);
                          }}>
                          <Link
                            data-url={
                              ctaItem.linkedPage.content.main.slug.current
                            }
                            to={`/${ctaItem.linkedPage.content.main.slug.current}/`}
                            onClick={() => {
                              segment('track', {
                                trackName: 'click',
                                text: ctaItem.linkedPage.content.main.title,
                                elementType: 'link',
                                elementID: `link-${ctaItem.linkedPage.content.main.slug.current}`,
                                colour: 'primary',
                                destination: `${window.location.origin}/${ctaItem.linkedPage.content.main.slug.current}/`,
                              });
                            }}>
                            {ctaItem.linkedPage.content.main.title}
                            <GetSubmenuItemsArrow />
                          </Link>
                          <GetSubmenuItems />
                        </li>
                      );
                    }
                    if (checkTypeNamePost) {
                      return (
                        <li
                          key={i}
                          data-item={ctaItem.linkedPage.content.main.title}
                          id={`${idPrefix}-item-${i}`}
                          className={`${
                            ctaItem.submenuItems.length > 0
                              ? 'menu-item menu-item--has-item'
                              : 'menu-item'
                          }`}>
                          <Link
                            data-url={ctaItem.linkedPage.slug.current}
                            to={`/${ctaItem.linkedPage.slug.current}/`}
                            onClick={() => {
                              segment('track', {
                                trackName: 'click',
                                text: ctaItem.linkedPage.title,
                                elementType: 'link',
                                elementID: `link-${ctaItem.linkedPage.slug.current}/`,
                                colour: 'primary',
                                destination: `${window.location.origin}/${ctaItem.linkedPage.slug.current}/`,
                              });
                            }}>
                            {ctaItem.linkedPage.title}
                            <GetSubmenuItemsArrow />
                          </Link>
                          <GetSubmenuItems />
                        </li>
                      );
                    }
                    if (checkTypeNameCategory) {
                      return (
                        <li
                          key={i}
                          data-item={ctaItem.linkedPage.title}
                          id={`${idPrefix}-item-${i}`}
                          className={`${
                            ctaItem.submenuItems.length > 0
                              ? 'menu-item menu-item--has-item'
                              : 'menu-item'
                          }`}>
                          <Link
                            data-url={ctaItem.linkedPage.slug.current}
                            to={`https://unohomeloans.com.au/categories/${ctaItem.linkedPage.slug.current}/`}
                            onClick={() => {
                              segment('track', {
                                trackName: 'click',
                                text: ctaItem.linkedPage.title,
                                elementType: 'link',
                                elementID: `link-${ctaItem.linkedPage.slug.current}/`,
                                colour: 'primary',
                                destination: `${window.location.origin}/categories/${ctaItem.linkedPage.slug.current}/`,
                              });
                            }}>
                            {ctaItem.linkedPage.title}
                            <GetSubmenuItemsArrow />
                          </Link>
                          <GetSubmenuItems />
                        </li>
                      );
                    }
                    if (checkTypeNameExternal) {
                      const SubItems = () => {
                        const [showResults, setShowResults] = useState(false);

                        if (isMobile) {
                          return (
                            <li
                              role="presentation"
                              key={i}
                              data-item={ctaItem.link.title}
                              id={`${idPrefix}-item-${i}`}
                              className={
                                ctaItem.submenuItems.length > 0
                                  ? `${'menu-item menu-item--has-item'} ${
                                      ctaItem.link.title
                                    }`
                                  : 'menu-item'
                              }>
                              <a
                                data-url={ctaItem.link.link}
                                href={`https://unohomeloans.com.au/${ctaItem.link.link}`}
                                onClick={(e) => {
                                  if (ctaItem.submenuItems.length > 0) {
                                    e.preventDefault();
                                    setShowResults(!showResults);
                                  }

                                  segment('track', {
                                    trackName: 'click',
                                    text: ctaItem.link.link,
                                    elementType: 'link',
                                    elementID: `link-${ctaItem.link.link}`,
                                    colour: 'primary',
                                    destination: `${window.location.origin}/${ctaItem.link.link}/`,
                                  });
                                }}>
                                {ctaItem.link.title}
                                <GetSubmenuItemsArrow />
                              </a>
                              {showResults ? <GetSubmenuItems /> : null}
                            </li>
                          );
                        }
                        return (
                          <li
                            key={i}
                            data-item={ctaItem.link.title}
                            id={`${idPrefix}-item-${i}`}
                            className={
                              ctaItem.submenuItems.length > 0
                                ? `${'menu-item menu-item--has-item'} ${
                                    ctaItem.link.title
                                  }`
                                : 'menu-item'
                            }>
                            <a
                              data-url={ctaItem.link.link}
                              href={
                                ctaItem.link.link.includes('houseprices')
                                  ? ctaItem.link.link
                                  : `https://unohomeloans.com.au/${ctaItem.link.link}`
                              }
                              onClick={() => {
                                setShowResults(!showResults);
                                segment('track', {
                                  trackName: 'click',
                                  text: ctaItem.link.link,
                                  elementType: 'link',
                                  elementID: `link-${ctaItem.link.link}`,
                                  colour: 'primary',
                                  destination: `${window.location.origin}${ctaItem.link.link}/`,
                                });
                              }}>
                              {ctaItem.link.title}
                              <GetSubmenuItemsArrow />
                            </a>
                            <GetSubmenuItems />
                          </li>
                        );
                      };

                      return (
                        <>
                          <SubItems />
                        </>
                      );
                    }
                    if (
                      checkTypeNameApplink &&
                      ctaItem.applink.select === 'CV2'
                    ) {
                      const withAppendCV2 = !isNil(ctaItem.applink.append)
                        ? ctaItem.applink.append
                        : '';
                      return (
                        <li
                          key={i}
                          data-item={ctaItem.applink.title}
                          id={`${idPrefix}-item-${i}`}
                          className={`${
                            ctaItem.submenuItems.length > 0
                              ? 'menu-item menu-item--has-item'
                              : 'menu-item'
                          }`}>
                          <a
                            data-url={ctaItem.applink.link}
                            href={`${CV2}${withAppendCV2}`}
                            onClick={() => {
                              segment('track', {
                                trackName: 'click',
                                text: ctaItem.applink.title,
                                elementType: 'link',
                                elementID: `link-${CV2}${withAppendCV2}`,
                                colour: 'primary',
                                destination: `${CV2}${withAppendCV2}`,
                              });
                            }}>
                            {ctaItem.applink.title}
                            <GetSubmenuItemsArrow />
                          </a>
                          <GetSubmenuItems />
                        </li>
                      );
                    }
                    return null;
                  })}
                </React.Fragment>
              );
            })}
          </React.Fragment>
        );
      })}
    </ul>
  );
};

export default Menu;
