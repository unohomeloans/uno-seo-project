import { Link } from 'gatsby';
import React from 'react';
import styled from 'styled-components';
import Layout from '../../Layout';
import SEO from '../../SEO';
import theme from '../../../styles/theme';
import { IndexerProps } from '../../../types/HousePrices.types';
import slugify from '../../../utils/slugify';
import Heading from '../Heading';

const LinksSection = styled.section`
  display: grid;
  margin: 0 auto;
  padding-left: 0.9375rem;
  padding-right: 0.9375rem;
  margin-bottom: ${theme.spacing.spacingLarge};
  max-width: 85rem;
  grid-template-columns: repeat(4, 1fr);
  column-gap: 32px;

  @media (max-width: 772px) {
    grid-template-columns: repeat(2, 1fr);
  }

  a {
    text-decoration: underline;
    font-family: ${theme.text.textBlack};
    text-transform: capitalize;
  }
`;

const Indexer: React.FC<IndexerProps> = ({
  state,
  lga,
  suburb,
  level,
  postcode,
  lgaLinks,
  suburbLinks,
  postcodeLinks,
}) => {
  const stateUppercased = state.toUpperCase();

  const seoTerm = `${suburb || ''} ${lga || ''} ${stateUppercased}`.trim();

  return (
    <Layout>
      <SEO
        title={`House prices in ${seoTerm}`}
        description={`Index of all house prices in ${seoTerm}`}
        bodyAttr={{
          id: `${seoTerm}-house-prices`,
          class: `${seoTerm}-house-prices-page`,
        }}
      />
      <Heading
        level={level}
        state={state}
        lga={lga || ''}
        postcode={postcode || 0}
        suburb={suburb || ''}
      />

      <LinksSection>
        {level === 'state' &&
          lgaLinks &&
          lgaLinks.map((lga) => (
            <h3 key={lga.lga_code}>
              <Link to={`/${slugify(state)}/${slugify(lga.lga_name)}`}>
                {lga.lga_name}
              </Link>
            </h3>
          ))}
        {level === 'lga' &&
          postcodeLinks &&
          postcodeLinks.map((postcode) => (
            <h3 key={postcode.lga_code}>
              <Link
                to={`/${slugify(state)}/${slugify(postcode.lga_name)}/${
                  postcode.postcode
                }`}>
                {postcode.postcode}
              </Link>
            </h3>
          ))}
        {level === 'postcode' &&
          suburbLinks &&
          suburbLinks.map((suburb) => (
            <h3 key={suburb.lga_code}>
              <Link
                to={`/${slugify(state)}/${slugify(suburb.lga_name)}/${
                  suburb.postcode
                }/${slugify(suburb.suburb)}`}>
                {suburb.suburb}
              </Link>
            </h3>
          ))}
      </LinksSection>
    </Layout>
  );
};

export default Indexer;
