import styled from 'styled-components';
import theme from '../../../styles/theme';

export const StyledHeading = styled.section`
  margin-top: ${theme.spacing.spacingLarge};
  margin-bottom: ${theme.spacing.spacingLarge};
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  h1 {
    font-size: 23px;
    text-align: center;
    max-width: 70%;

    a,
    span {
      margin: 0 0.5rem;
    }
    a {
      font-family: ${theme.text.textBlack};
      text-decoration: underline;
      text-transform: capitalize;
    }
  }

  .locality {
    text-transform: capitalize;
  }
`;
