import { Link } from 'gatsby';
import React from 'react';
import { HeadingProps } from '../../../types/HousePrices.types';
import slugify from '../../../utils/slugify';
import { StyledHeading } from './Heading.styled';

const Heading: React.FC<HeadingProps> = ({
  level,
  state,
  suburb,
  postcode,
  lga,
}) => {
  const stateUppercased = state.toUpperCase();
  const stateLowercased = state.toLowerCase();
  const slugifiedLGA = slugify(lga);

  return (
    <StyledHeading>
      <h1>
        <Link to="/#house-prices-content">{stateUppercased}</Link>
        <span> / </span>
        {level === 'state' && <strong>Choose an LGA</strong>}
        {(level === 'lga' || level === 'postcode' || level === 'locality') && (
          <>
            <Link to={`/${stateLowercased}`}>{lga}</Link>
            <span> / </span>
          </>
        )}
        {level === 'lga' && <strong>Choose a postcode</strong>}
        {(level === 'postcode' || level === 'locality') && (
          <>
            <Link to={`/${stateLowercased}/${slugifiedLGA}`}>{postcode}</Link>
            <span> / </span>
          </>
        )}
        {level === 'postcode' && <strong>Choose a suburb</strong>}
        <strong className="locality">{suburb}</strong>
      </h1>
    </StyledHeading>
  );
};

export default Heading;
