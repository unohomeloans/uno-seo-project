import styled, { keyframes, css } from 'styled-components';
import pxToRem from '../../../../utils/pxToRem';
import theme from '../../../../styles/theme';

export interface InputTypeAheadV2Props {
  /** Unique Id of the component. */
  idSelector: string;
  /** List of space separated CSS classes */
  additionalClasses?: string;
  /** Value of the input field */
  textValue: string;
  /** Array of List items */
  listItems: ReadonlyArray<{
    display: string;
    value: string;
    [key: string]: string | number | boolean | null;
  }>;
  /** TextLabel of the component */
  textLabel?: string;
  /**  Placeholder to show in the input element */
  placeHolder?: string;
  error?: string;
  /** Flag to show clear input button */
  showClearIcon?: boolean;
  /** Flag to show back button inside of the input */
  showBackButton?: boolean;
  /** Flag to show the input Label */
  showLabel?: boolean;
  /** Text for screenreaders */
  screenReaderInputLabel?: string;
  /** Flag to highlight last row */
  highlightLastRow?: boolean;
  /** Flag for the full story */
  sensitive?: boolean;
  /** Callback function for onChange */
  onChange?: (element: { target: { value: string } }) => void;
  /** Callback function for onSelect */
  onSelect?: (selectedItem: {
    display: string;
    value: string;
    [key: string]: string | number | boolean | null;
  }) => void;
}

export interface InputTypeAheadV2State {
  textFieldValue: string;
  showDropDown: boolean;
  showClearIcon: boolean;
  showBackButton: boolean;
  hasFocus: boolean;
}

const animationFadeIn = keyframes`
  0% { opacity: 0; }
  100% { opacity: 1; }
`;

export const InputTypeAheadInputDropDownBack = styled.button<
  Partial<InputTypeAheadV2Props>
>`
  position: absolute;
  top: 55%;
  transform: translateY(-50%);
  left: ${pxToRem(10)};
  height: ${pxToRem(24)};
  width: ${pxToRem(24)};
  background: transparent;
  border: none;
  padding: 0;

  svg {
    path {
      fill: ${theme.colors.colorPrimary};
    }
  }
`;

export const InputTypeAheadInput = styled.input`
  background: ${theme.colors.colorWhite};
  border: ${pxToRem(1)} solid ${theme.colors.colorGrayDarkSecondary};
  font-size: ${theme.text.fontSizeSmaller};
  font-family: ${theme.text.textBook}, sans-serif;
  border-radius: 0.5rem;
  padding: 0 1.5rem;
  height: ${pxToRem(60)};
  width: 100%;
  overflow: hidden;
  appearance: none;
  transition: all 0.2s linear;

  &::-webkit-input-placeholder {
    color: ${theme.colors.colorGrayDarkSecondary};
  }

  &:focus,
  &:active {
    outline: 0;
    background: ${theme.colors.colorWhite};
    border: ${pxToRem(1)} solid ${theme.colors.colorPrimary};
    box-shadow: 0 ${pxToRem(1)} ${pxToRem(3)} 0 rgba(40, 64, 83, 0.2),
      0 0 ${pxToRem(10)} 0 rgba(20, 178, 161, 0.2);
  }
`;

export const InputTypeAheadInputDropDown = styled.div<
  Partial<InputTypeAheadV2Props>
>`
  width: 100%;

  .c-card-typeahead {
    top: ${pxToRem(65)};
    box-shadow: none;
  }
`;

export const InputTypeAheadInputDropDownClear = styled.button<
  Partial<InputTypeAheadV2Props>
>`
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  right: ${pxToRem(15)};
  height: ${pxToRem(20)};
  width: ${pxToRem(20)};
  border: none;
  padding: 0;
  background: transparent;

  ${({ showLabel }) =>
    showLabel &&
    css`
      top: 50%;
    `}

  svg {
    path {
      fill: ${theme.colors.colorPrimary};
    }
  }
`;

export const InputTypeAheadWrapper = styled.div<Partial<InputTypeAheadV2Props>>`
  display: flex;
  flex-direction: column;
  flex: 1;
  align-items: flex-start;
  position: relative;

  &.TypeAheadInputDropdown__open {
    ${({ showBackButton }) =>
      showBackButton &&
      css`
        input {
          padding: 0 ${theme.spacing.spacingXSmall} 0
            ${theme.spacing.spacingLarge};

          &:focus,
          &:active {
            padding: 0 ${theme.spacing.spacingXSmall} 0
              ${theme.spacing.spacingLarge};
          }
        }
      `}
  }

  &.TypeAheadInputDropdown__default {
    input {
      &:focus,
      &:active {
        padding: 0 ${theme.spacing.spacingXSmall};
      }
    }
  }

  &.TypeAheadInputDropdown__open {
    .c-card-typeahead {
      opacity: 1;
      animation: ${animationFadeIn} 0.5s linear;
      margin: auto;
    }

    ${InputTypeAheadInputDropDownBack},
    ${InputTypeAheadInputDropDownClear} {
      opacity: 1;
      animation: ${animationFadeIn} 0.5s linear;
      margin: auto;
    }
  }
`;

export const InputTypeAheadLabel = styled.label`
  font-family: ${theme.text.fontMedium};
  font-size: ${theme.text.fontSizeSmaller};
  display: inline-block;
  vertical-align: baseline;
  margin-bottom: ${theme.spacing.spacingXXSmall};
  line-height: ${theme.spacing.spacingXSmall};
`;
