import React from 'react';
import pushIfNotExist from '../../../../../utils/pushIfNotExist';

interface FormCardProps {
  additionalClasses?: string[];
  asWrapperOnly?: boolean;
  formItem?: any;
  formItems?: any;
}

const FormCard: React.FC<FormCardProps> = ({
  formItem,
  formItems,
  additionalClasses,
  asWrapperOnly,
}) => {
  let classNames = additionalClasses;

  if (classNames === null || !classNames) {
    classNames = [];
  }

  const defaultClassName = 'c-card-form';
  pushIfNotExist(classNames, defaultClassName);

  if (!asWrapperOnly) {
    pushIfNotExist(classNames, 'c-card-form-input');
  }

  const allClassNames = classNames.join(' ').trim();

  return (
    <div className={allClassNames}>
      {formItem}
      {formItems}
    </div>
  );
};

FormCard.defaultProps = {
  asWrapperOnly: false,
};

export default FormCard;
