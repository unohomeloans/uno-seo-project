import { ReactNode } from 'react';
import styled from 'styled-components';
import pxToRem from '../../../../../utils/pxToRem';
import theme from '../../../../../styles/theme';

export interface ListTypeAheadV2Props {
  /** Unique Id of the component. */
  idSelector: string;
  /** List of space separated CSS classes */
  additionalClasses?: string;
  /** List of <li> items */
  listItems: ReactNode[];
  /** Label for the "Can't find your address link" */
  additionalItem?: ReactNode[];
  /** Flag to highlight the last row */
  highlightLastRow?: boolean;
}

export const ListTypeAheadWrapper = styled.div`
  display: flex;
  flex-direction: column;
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

export const ListTypeAheadUl = styled.ul`
  flex: auto;
`;

const ListTypeAheadUlItemLast = '#33777233;';
const ListTypeAheadUlItemLastHover = '#526675;';

export const ListTypeAheadUlItemLastIcon = styled.span<
  Partial<ListTypeAheadV2Props>
>`
  padding: 0;

  &.ListTypeAhead__default--icon {
    display: none;
    padding: 0;
  }
`;

export const ListTypeAheadUlItem = styled.li<Partial<ListTypeAheadV2Props>>`
  display: flex;
  align-items: center;
  min-width: 0;
  max-height: none;
  border-bottom: solid ${pxToRem(1)} ${theme.colors.colorGrayLight};
  transition: background 0.2s ease;

  a,
  div,
  span {
    display: inline-block;
    width: 100%;
    color: ${theme.colors.colorGrayDark};
    font-size: ${theme.text.fontSizeSmaller};
    padding: ${theme.spacing.spacingXXSmall} ${theme.spacing.spacingXSmall};
  }

  &:hover {
    background: ${theme.colors.colorPrimary};
    cursor: pointer;

    a,
    div,
    span {
      color: ${theme.colors.colorWhite};
    }
  }

  ${({ highlightLastRow }) =>
    highlightLastRow
      ? `
    &:last-child {
      background: ${ListTypeAheadUlItemLast};
      position: relative;
      padding: ${theme.spacing.spacingXXSmall} 0;

      > span {
        text-decoration: underline;

        &:last-child {
          padding-left: ${pxToRem(6)};
        }
        
        &:hover {
          color: ${ListTypeAheadUlItemLastHover};
        }
      }

      padding-left: ${pxToRem(10)};


      &:hover {
        a,
        div,
        span {
          color: ${ListTypeAheadUlItemLastHover};
        }
      }


      ${ListTypeAheadUlItemLastIcon} {
        display: flex;
        width: auto;
      }
    }
  `
      : ''}
`;
