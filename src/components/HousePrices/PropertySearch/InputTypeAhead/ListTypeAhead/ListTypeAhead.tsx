import React from 'react';
import isNil from 'lodash/isNil';
import pushIfNotExist from '../../../../../utils/pushIfNotExist';
import {
  ListTypeAheadV2Props,
  ListTypeAheadWrapper,
  ListTypeAheadUl,
  ListTypeAheadUlItem,
  ListTypeAheadUlItemLastIcon,
} from './ListTypeAhead.styled';

class ListTypeAheadV2 extends React.Component<ListTypeAheadV2Props> {
  render() {
    const {
      idSelector,
      additionalClasses,
      listItems,
      additionalItem,
      highlightLastRow,
    } = this.props;

    const idPrefix = !isNil(idSelector) ? idSelector : 'list-typeahead-v2';

    const classNames = ['ListTypeAhead__default'];

    if (additionalClasses) pushIfNotExist(classNames, additionalClasses);

    const allClassNames = classNames.join(' ').trim();

    const uniqueNumber = Math.floor(Math.random() * 255);

    const htmlItems = listItems.map((item, index) => {
      return (
        <ListTypeAheadUlItem
          id={`${idPrefix}-list-item`}
          key={`${idPrefix}_${uniqueNumber}_${index}`}
          highlightLastRow={highlightLastRow}>
          {highlightLastRow && (
            <ListTypeAheadUlItemLastIcon className="ListTypeAhead__default--icon">
              <svg width="20" height="20">
                <path
                  d="M10 0C4.48 0 0 4.48 0 10s4.48 10 10 10 10-4.48 10-10S15.52 0 10 0zm.062 9c.637 0 1.061.237 1.061.52v5.726c0 .236-.424.33-.955.33h-.106c-.637 0-1.062-.047-1.062-.33V9.473C9 9.19 9.425 9 10.062 9zm0-4c.318 0 .53.107.743.322.212.215.318.43.318.752s-.106.537-.318.752c-.425.43-1.062.322-1.487 0C9.106 6.61 9 6.396 9 6.074s.106-.537.318-.752c.213-.215.425-.322.744-.322z"
                  fill="#2577D7"
                  fillRule="evenodd"
                />
              </svg>
            </ListTypeAheadUlItemLastIcon>
          )}
          {item}
        </ListTypeAheadUlItem>
      );
    });

    return (
      <ListTypeAheadWrapper
        id={idPrefix}
        className={allClassNames}
        key={`${idPrefix + uniqueNumber}`}>
        <ListTypeAheadUl id={`${idPrefix}-list`}>
          {additionalItem}
          {htmlItems}
        </ListTypeAheadUl>
      </ListTypeAheadWrapper>
    );
  }
}

export default ListTypeAheadV2;
