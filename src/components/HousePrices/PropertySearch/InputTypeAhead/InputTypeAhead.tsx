import React from 'react';
import isNil from 'lodash/isNil';
import debounce from 'lodash/debounce';
import pushIfNotExist from '../../../../utils/pushIfNotExist';
import FormCard from './FormCard';
import ListTypeAhead from './ListTypeAhead';
import {
  InputTypeAheadV2Props,
  InputTypeAheadV2State,
  InputTypeAheadWrapper,
  InputTypeAheadLabel,
  InputTypeAheadInput,
  InputTypeAheadInputDropDown,
  InputTypeAheadInputDropDownClear,
  InputTypeAheadInputDropDownBack,
} from './InputTypeAhead.styled';

class Search extends React.Component<
  InputTypeAheadV2Props,
  InputTypeAheadV2State
> {
  inputTypeAheadElementRef: React.RefObject<HTMLInputElement>;
  // eslint-disable-next-line @typescript-eslint/ban-types
  autocompleteSearchDebounced: <T extends Function>(
    search: string,
    cb?: T
  ) => void;

  constructor(props: InputTypeAheadV2Props) {
    super(props);
    this.state = {
      textFieldValue: !isNil(props.textValue) ? props.textValue : '',
      showDropDown: false,
      showClearIcon: false,
      showBackButton: false,
      hasFocus: false,
    };

    // Get a handle of the input element to help with the focus
    this.inputTypeAheadElementRef = React.createRef();
    this.autocompleteSearchDebounced = debounce(this.triggerOnChange, 500);
  }

  static defaulProps = {
    placeHolder: '',
    error: '',
    showClearIcon: false,
    showBackButton: false,
    showLabel: true,
    hasFocus: false,
  };

  inputTypeAheadContainerRef: HTMLDivElement | null | undefined;

  componentDidMount() {
    const { handleClick, handleEscKeyPress } = this;
    document.addEventListener('click', handleClick, false);
    document.addEventListener('keydown', handleEscKeyPress, false);
  }

  componentWillUnmount() {
    const { handleClick, handleEscKeyPress } = this;
    document.removeEventListener('click', handleClick, false);
    document.removeEventListener('keydown', handleEscKeyPress, false);
  }

  componentDidUpdate(
    prevProps: InputTypeAheadV2Props,
    prevState: InputTypeAheadV2State
  ) {
    const hasTextValueChanged = this.props.textValue !== prevProps.textValue;

    if (
      hasTextValueChanged &&
      prevState.textFieldValue !== this.props.textValue
    ) {
      this.setState({
        textFieldValue: this.props.textValue,
      });
    }
  }

  handleClearInput = () => {
    const { onChange } = this.props;
    const value = '';

    this.setState(
      {
        textFieldValue: '',
        showDropDown: false,
        showBackButton: false,
        hasFocus: false,
      },
      () => {
        this.inputTypeAheadElementRef.current &&
          this.inputTypeAheadElementRef.current!.blur();
        onChange &&
          onChange({
            target: {
              value,
            },
          });
      }
    );
  };

  triggerOnChange = (value: any) => {
    const { onChange = () => null } = this.props;
    onChange({
      target: {
        value,
      },
    });
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;

    this.setState(
      {
        textFieldValue: value,
      },
      () => {
        this.autocompleteSearchDebounced(value);
      }
    );
  };

  handleInputFocus = () => {
    this.setState({
      showDropDown: true,
      showClearIcon: true,
      showBackButton: true,
      hasFocus: true,
    });
  };

  handleSelection = (item: { display: string; value: string }) => {
    const { onSelect } = this.props;

    this.setState(
      {
        textFieldValue: item.display,
        showDropDown: false,
        showBackButton: false,
        hasFocus: false,
      },
      () => {
        onSelect && onSelect(item);
      }
    );
  };

  handleClick = (event: any) => {
    const { inputTypeAheadContainerRef } = this;

    if (
      !isNil(inputTypeAheadContainerRef) &&
      inputTypeAheadContainerRef.contains(event.target)
    ) {
      this.setState({ showDropDown: true });

      return;
    }

    this.setState({
      showDropDown: false,
      showBackButton: false,
      hasFocus: false,
    });
  };

  handleEscKeyPress = (event: any) => {
    if (event.keyCode === 27) {
      this.setState({
        showDropDown: false,
        showBackButton: false,
        hasFocus: false,
      });
    }
  };

  handleBackClick = () => {
    // This is also triggers the animation back to the screen the user landed.
    this.setState(
      {
        textFieldValue: '',
        showDropDown: false,
        showBackButton: false,
        hasFocus: false,
      },
      () => {
        this.inputTypeAheadElementRef.current &&
          this.inputTypeAheadElementRef.current!.blur();
      }
    );
  };

  render() {
    const {
      idSelector,
      additionalClasses,
      listItems,
      placeHolder,
      showClearIcon,
      screenReaderInputLabel,
      highlightLastRow,
      sensitive,
      showLabel,
      textLabel,
    } = this.props;

    const {
      textFieldValue,
      showDropDown,
      showBackButton,
      hasFocus,
    } = this.state;

    const {
      handleChange,
      handleInputFocus,
      handleSelection,
      handleClearInput,
      handleBackClick,
    } = this;

    const classNames = [
      'form-item',
      'TypeAheadInput__default',
      showDropDown
        ? 'TypeAheadInputDropdown__open'
        : 'TypeAheadInputDropdown__default',
    ];

    const idPrefix = !isNil(idSelector) ? idSelector : 'input-typeahead-v2';

    if (additionalClasses) pushIfNotExist(classNames, additionalClasses);

    const allClassNames = classNames.join(' ').trim();

    const showResultsDiv =
      !isNil(listItems) && listItems.length > 0 && showDropDown;

    return (
      <InputTypeAheadWrapper
        id={idPrefix}
        className={allClassNames}
        ref={(node) => {
          this.inputTypeAheadContainerRef = node;
        }}
        showBackButton>
        {showLabel && (
          <InputTypeAheadLabel
            id={`${idPrefix}-label`}
            htmlFor={`${idPrefix}-input`}
            className="label">
            {textLabel}
          </InputTypeAheadLabel>
        )}
        <InputTypeAheadInput
          ref={this.inputTypeAheadElementRef}
          id={`${idPrefix}-input`}
          type="text"
          aria-label={screenReaderInputLabel}
          className={`${idPrefix}-input-default`}
          placeholder={placeHolder ? placeHolder : ''}
          value={textFieldValue}
          autoComplete="false"
          onChange={handleChange}
          onFocus={handleInputFocus}
        />
        {showBackButton && (
          <InputTypeAheadInputDropDownBack
            onClick={handleBackClick}
            showLabel={showLabel}>
            <svg width="17" height="16" xmlns="http://www.w3.org/2000/svg">
              <title>Go Back</title>
              <path
                d="M9.753.341a1 1 0 01-.094 1.412L3.66 7H16a1 1 0 010 2H3.661l5.998 5.247a1 1 0 01-1.318 1.506l-8-7-.014-.014a1.006 1.006 0 01-.061-.06l.075.074a1.012 1.012 0 01-.26-.359 1.004 1.004 0 01.26-1.147l8-7a1 1 0 011.412.094z"
                fill="#337772"
                fillRule="evenodd"
              />
            </svg>
          </InputTypeAheadInputDropDownBack>
        )}
        {showClearIcon && hasFocus && textFieldValue && (
          <InputTypeAheadInputDropDownClear
            onClick={handleClearInput}
            showLabel={showLabel}>
            <svg width="20" height="20" xmlns="http://www.w3.org/2000/svg">
              <title>Clear Field</title>
              <path
                d="M17.071 2.929c3.906 3.906 3.905 10.237 0 14.142-3.905 3.906-10.237 3.906-14.142 0-3.906-3.905-3.906-10.236 0-14.142 3.905-3.905 10.237-3.905 14.142 0zm-3.535 3.536a1 1 0 00-1.327-.078l-.088.078-2.12 2.12-2.122-2.12a1 1 0 00-1.327-.078l-.087.078a1 1 0 000 1.414L8.586 10l-2.121 2.121a1 1 0 001.327 1.492l.087-.077L10 11.414l2.121 2.122a1 1 0 001.327.077l.088-.077a1 1 0 000-1.415l-2.122-2.12 2.122-2.122a1 1 0 000-1.414z"
                fillRule="evenodd"
              />
            </svg>
          </InputTypeAheadInputDropDownClear>
        )}
        {showResultsDiv && (
          <InputTypeAheadInputDropDown showLabel={showLabel}>
            <FormCard
              additionalClasses={['c-card-no-spacing', 'c-card-typeahead']}
              asWrapperOnly
              formItem={
                <ListTypeAhead
                  idSelector={idPrefix}
                  highlightLastRow={highlightLastRow}
                  listItems={listItems.map((item) => {
                    return (
                      <span
                        id={`${idPrefix}-item-${item.value}`}
                        key={item.value}
                        tabIndex={0}
                        onClick={() => {
                          handleSelection(item);
                        }}
                        onKeyPress={(
                          event: React.KeyboardEvent<HTMLDivElement>
                        ) => {
                          event.preventDefault();
                          handleSelection(item);
                        }}>
                        {item.display}
                      </span>
                    );
                  })}
                />
              }
            />
          </InputTypeAheadInputDropDown>
        )}
      </InputTypeAheadWrapper>
    );
  }
}

export default Search;
