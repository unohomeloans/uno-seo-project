import React, { useEffect, useState } from 'react';
import { graphql, useStaticQuery, navigate } from 'gatsby';
import Fuse from 'fuse.js';
import styled from 'styled-components';
import capitalise from '../../../utils/capitalise';
import slugify from '../../../utils/slugify';
import theme from '../../../styles/theme';
import InputTypeAhead from './InputTypeAhead';

interface SuburbList {
  [key: string]: string;
  value: string;
  display: string;
}

const getSuburbList = (
  searchResults: Fuse.FuseResult<SearchItem>[]
): SuburbList[] => {
  return searchResults
    .map((searchResult) => {
      const value = `/${slugify(searchResult.item.state)}/${slugify(
        searchResult.item.lga_name
      )}/${searchResult.item.postcode}/${slugify(searchResult.item.suburb)}`;

      const display = `${capitalise(searchResult.item.suburb)} ${
        searchResult.item.state
      } ${searchResult.item.postcode}`;

      const key = `${searchResult.refIndex}-${searchResult.item.suburb}`;

      return {
        key,
        value,
        display,
      };
    })
    .filter(
      (element, index, array) =>
        index === array.findIndex((j) => j.display === element.display)
    );
};

interface SearchItem {
  postcode: number;
  suburb: string;
  state: string;
  lga_name: string;
}

interface SearchListGraphQL {
  allDataJson: {
    edges: {
      node: {
        data: SearchItem[];
      };
    }[];
  };
}

const LoadingText = styled.section`
  background: #e7f7f5;
  margin-top: 2rem;
  border-radius: 10px;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  p {
    color: ${theme.colors.colorPrimary};
    text-align: center;
    font-weight: bold;
    margin-top: 0;
    padding-top: 0;
    margin-bottom: 0;
  }
`;

const PropertySearch: React.FC = () => {
  const searchList = useStaticQuery<SearchListGraphQL>(graphql`
    {
      allDataJson(skip: 1) {
        edges {
          node {
            data {
              postcode
              suburb
              state
              lga_name
            }
          }
        }
      }
    }
  `);

  const list = searchList.allDataJson.edges[0].node.data;
  const [searchValue, setSearchValue] = useState<string>('');
  const [suburbList, setSuburbList] = useState<SuburbList[]>([]);
  const [fuseInst, setFuseInst] = useState<Fuse<SearchItem>>();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    setFuseInst(
      new Fuse(list, {
        keys: ['postcode', 'suburb'],
        shouldSort: true,
        threshold: 0.1,
      })
    );
  }, [list]);

  const handleSearch = (element: any) => {
    const searchInput = element.target.value;
    setSearchValue(element.target.value);

    if (fuseInst) {
      const searchResults = fuseInst.search(searchInput, { limit: 5 });

      if (searchResults.length === 0) {
        setSuburbList([]);
      } else {
        setSuburbList(getSuburbList(searchResults));
      }
    }
  };

  return (
    <>
      <InputTypeAhead
        showClearIcon
        showBackButton
        placeHolder="Search for a suburb or postcode..."
        idSelector="property-seach"
        listItems={suburbList}
        textValue={searchValue}
        onChange={handleSearch}
        onSelect={(selectedItem) => {
          setIsLoading(true);
          navigate(selectedItem.value);
        }}
      />
      {isLoading && (
        <LoadingText>
          <p>Loading please wait...</p>
        </LoadingText>
      )}
    </>
  );
};

export default PropertySearch;
