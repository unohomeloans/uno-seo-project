import React from 'react';
import { graphql, useStaticQuery, Link } from 'gatsby';
import { GatsbyImage } from 'gatsby-plugin-image';

const HeaderLogo: React.FC = () => {
  const data = useStaticQuery(graphql`
    {
      sanitySiteSettings {
        logo {
          asset {
            url
          }
          alt
        }
      }
    }
  `);

  return (
    <div className="header-brand">
      <div className="header-brand--logo">
        <Link
          to="https://unohomeloans.com.au"
          data-url="https://unohomeloans.com.au">
          <img
            src={data.sanitySiteSettings?.logo?.asset.url}
            alt={data.sanitySettings?.logo?.alt}
            style={{ width: '100%', maxWidth: '90px' }}
          />
        </Link>
      </div>
    </div>
  );
};

export default HeaderLogo;
