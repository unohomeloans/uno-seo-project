import React from 'react';
import removeAccents from '../../utils/removeAccents';
import segment from '../../utils/segment';

interface CTAsProps {
  idSelector?: string;
}

const CTAs: React.FC<CTAsProps> = ({ idSelector }) => {
  const idPrefix = idSelector || `header-ctas`;

  const CV2 = process.env.GATSBY_MY_SANITY_CV2;

  return (
    <div id={idPrefix} className="header-ctas">
      <div id={`${idPrefix}-item-login`} className="header-ctas--item">
        <a
          href={`${CV2}login`}
          className="button"
          onClick={() => {
            segment('track', {
              trackName: 'click',
              text: 'Login',
              elementType: 'button',
              elementID: `button-${removeAccents('Login')}`,
              colour: 'primary',
              destination: `${window.location.href}home-loans/#sign-in`,
            });
          }}>
          Login
        </a>
      </div>
      <div id={`${idPrefix}-item-getstarted`} className="header-ctas--item">
        <a
          href="https://unohomeloans.com.au/find-me-homeloan"
          data-link="https://unohomeloans.com.au/find-me-homeloan"
          className="button button--primary"
          onClick={() => {
            segment('track', {
              trackName: 'click',
              text: 'Get Started',
              elementType: 'button',
              elementID: `button-${removeAccents('Get Started')}`,
              colour: 'primary',
              destination: '/find-me-homeloan',
            });
          }}>
          Get Started
        </a>
      </div>
    </div>
  );
};

export default CTAs;
