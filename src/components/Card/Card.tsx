import React from 'react';
import styled from 'styled-components';
import pxToRem from '../../utils/pxToRem';

const StyledCard = styled.div`
  background: ${(props) => props.theme.colors.colorWhite};
  box-shadow: 0 0 ${pxToRem(20)} ${pxToRem(4)} rgba(0, 0, 0, 0.03),
    0 ${pxToRem(40)} ${pxToRem(40)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(16)} ${pxToRem(16)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(8)} ${pxToRem(8)} 0 rgba(40, 64, 83, 0.03),
    0 ${pxToRem(4)} ${pxToRem(4)} 0 rgba(0, 0, 0, 0.05);
  border-radius: ${pxToRem(12)};
  height: 100%;
`;

const Card: React.FC = ({ children }) => (
  <StyledCard className="c-card">{children}</StyledCard>
);

export default Card;
