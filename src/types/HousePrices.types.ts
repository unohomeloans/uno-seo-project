import { Dayjs } from 'dayjs';

export interface Locality {
  lga_code: number;
  lga_name: string;
  postcode: number;
  suburb: string;
  state: string;
}

export interface LGABroker {
  lga_name: string;
  broker_name: string;
  broker_image_url: string;
  broker_utm_param: string;
}

export interface HousePricesGraphQL {
  allDataJson: {
    edges: {
      node: {
        data: Locality[] & LGABroker[];
      };
    }[];
  };
}

export interface AddedLocalities {
  state: string[];
  lga: Locality[];
  postcode: Locality[];
  suburb: Locality[];
}

export interface IDSStats {
  dates: string[];
  price: number[];
  rents: number[];
  salecounts: number[];
  tom: number[];
  vee: number[];
  yields: number[];
}

export interface IDSIndices {
  series: {
    [locality: string]: {
      price: number[];
      rents: number[];
      salecounts: number[];
      tom: number[];
      vee: number[];
      yields: number[];
    };
  };
}

export interface IDSResponse {
  interval: string;
  dates: string[];
  unit: IDSIndices;
  house: IDSIndices;
}

export interface SalesOverview {
  medianPropertyValue: number;
  deltaByMonth: {
    change: string;
    amount: number;
  };
  deltaByYear: {
    change: string;
    amount: number;
  };
  rentalDemandChangeByMonth: string;
  yieldsDeltaByMonth: {
    change: string;
    from: number;
    to: number;
  };
  numPropertiesSoldDeltaByQuarter: {
    change: string;
    from: number;
    to: number;
  };
  numPropertiesSoldDeltaByFiveYearQuarters: {
    change: string;
    from: number;
    indication: string;
  };
}

interface Chart {
  data: {
    date: string;
    value: number;
  }[];
}

interface ChangeFromAmount {
  change: string;
  from: number;
  amount: number;
}

export interface VendorExpectationError {
  change: 'more' | 'less';
  currentMonth: number;
  chart: Chart;
}

export interface TimeOnMarket {
  change: 'up' | 'down' | '';
  consecutiveMonths: number;
  sinceConsecutiveMonth: string;
  deltaByMonth: {
    change: string;
    from: number;
    to: number;
  };
  chart: Chart;
}

export interface SaleCounts {
  change: 'lowest' | 'highest' | 'between';
  average: number;
  averageDiff: number;
  deltaByMonth: {
    change: string;
    from: number;
    to: number;
  };
  deltaBy5YearLow: {
    change: string;
    from: {
      quarter: string;
      salecounts: number;
    };
    amount: number;
  };
  deltaBy5YearHigh: {
    change: string;
    from: {
      quarter: string;
      salecounts: number;
    };
    amount: number;
  };
  chart: Chart;
}

export interface RentalYield {
  change: 'increased' | 'decreased' | '';
  consecutiveMonths: number;
  currentMonth: number;
  deltaBy12MonthLow: {
    change: string;
    from: {
      date: Dayjs;
      yield: number;
    };
    amount: number;
  };
  deltaBy12MonthHigh: {
    change: string;
    from: {
      date: Dayjs;
      yield: number;
    };
    amount: number;
  };
  chart: Chart;
}

export interface RentalCost {
  currentMonth: number;
  change: 'highest' | 'lowest' | 'between';
  deltaBy12MonthLow: {
    change: string;
    from: {
      date: Dayjs;
      rent: number;
    };
    amount: number;
  };
  deltaBy12MonthHigh: {
    change: string;
    from: {
      date: Dayjs;
      rent: number;
    };
    amount: number;
  };
  deltaByMonth: ChangeFromAmount;
  chart: Chart;
}

export interface MedianPrice {
  lastMonthPrice: number;
  chart: Chart;
}

export interface CalculatedStats {
  salesOverview: SalesOverview;
  medianPrice: MedianPrice;
  rentalCost: RentalCost;
  rentalYield: RentalYield;
  saleCounts: SaleCounts;
  timeOnMarket: TimeOnMarket;
  vendorExpectationError: VendorExpectationError;
}

export interface HouseUnitCalculatedStats {
  house: CalculatedStats;
  unit: CalculatedStats;
}

export type IndexLevel = 'state' | 'lga' | 'postcode' | 'suburb' | 'locality';

export interface IndexerProps {
  level: IndexLevel;
  state: string;
  lga?: string;
  postcode?: number;
  suburb?: string;
  lgaLinks?: Locality[];
  suburbLinks?: Locality[];
  postcodeLinks?: Locality[];
}

export interface LocalityProps {
  pageContext: {
    state: string;
    lga: string;
    broker: LGABroker;
    postcode: number;
    suburb: string;
    date: Dayjs;
    lastMonthDate: Dayjs;
    lastYearDate: Dayjs;
    house: CalculatedStats;
    unit: CalculatedStats;
  };
}

export interface HeadingProps {
  level: IndexLevel;
  state: string;
  postcode: number;
  suburb: string;
  lga: string;
}

export interface HousePricesProps {
  pageContext: IndexerProps;
}
