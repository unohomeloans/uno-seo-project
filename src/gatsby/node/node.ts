import { config } from 'dotenv';
// import fs from 'fs-extra';
import { CreateResolversArgs, GatsbyNode } from 'gatsby';
import createHousePricesPages from './createHousePricesPages';

config({ path: `.env.${process.env.NODE_ENV || 'development'}` });

export const createPages: GatsbyNode['createPages'] = async (args) => {
  await createHousePricesPages({ ...args }, args.cache);
};

// export const onPostBuild: GatsbyNode['onPostBuild'] = async (args) => {
//   if (fs.existsSync('.cache')) {
//     args.reporter.info(
//       'Copying cache to local cache folder. This will take some time...'
//     );
//     fs.copySync('.cache', 'cache', { recursive: true });
//   }
// };

// export const onPreBootstrap: GatsbyNode['onPreBootstrap'] = async (args) => {
//   if (fs.existsSync('cache')) {
//     args.reporter.info(
//       'Copying local cache to .cache. This will take some time...'
//     );
//     fs.copySync('cache', '.cache', { recursive: true });
//   }
// };

export const createResolvers: GatsbyNode['createResolvers'] = async ({
  createResolvers,
}: CreateResolversArgs) => {
  const resolvers = {
    SanityCategory: {
      posts: {
        type: ['SanityPost'],
        resolve(source: any, _: any, context: any) {
          return context.nodeModel.runQuery({
            type: 'SanityPost',
            query: {
              filter: {
                categories: {
                  elemMatch: {
                    _id: {
                      eq: source._id,
                    },
                  },
                },
              },
            },
          });
        },
      },
    },
  };

  createResolvers(resolvers);
};
