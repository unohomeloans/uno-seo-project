import { SaleCounts } from '../../../types/HousePrices.types';
import calculateDeltaPercentage from '../../../utils/localityData/calculateDeltaPercentage';
import getQuarter from '../../../utils/localityData/getQuarter';

export default (
  dates: string[],
  currentMonthDate: Date,
  salecounts: number[]
): SaleCounts => {
  const datesWithQuarters = dates.map((d, index) => {
    const date = new Date(d);
    const quarter = getQuarter(date);
    return {
      date,
      index,
      quarter,
    };
  });

  const currentQuarterDates = datesWithQuarters.filter(
    (quarterDate) =>
      quarterDate.quarter === getQuarter(currentMonthDate) &&
      quarterDate.date.getFullYear() === currentMonthDate.getFullYear()
  );

  const lastQuarterDates = datesWithQuarters.filter((quarterDate) => {
    const absoluteQuarterValue = getQuarter(currentMonthDate) - 1;
    const relativeQuarterValue =
      absoluteQuarterValue === 0 ? 4 : absoluteQuarterValue;

    const quarterDateYear = quarterDate.date.getFullYear();
    const currentMonthYear = currentMonthDate.getFullYear();

    return (
      quarterDate.quarter === relativeQuarterValue &&
      (quarterDateYear === currentMonthYear ||
        quarterDateYear === currentMonthYear - 1)
    );
  });

  let currentQuarterPropertiesSold = 0;
  let lastQuarterPropertiesSold = 0;

  currentQuarterDates.forEach((currentQuarterDate) => {
    currentQuarterPropertiesSold += salecounts[currentQuarterDate.index];
  });

  lastQuarterDates.forEach((lastQuarterDate) => {
    lastQuarterPropertiesSold += salecounts[lastQuarterDate.index];
  });

  const lastCurrentQuarterPropertiesSoldDeltaChange =
    currentQuarterPropertiesSold === lastQuarterPropertiesSold
      ? 'the same'
      : currentQuarterPropertiesSold > lastQuarterPropertiesSold
      ? 'up'
      : 'down';

  const quartersByYearRange: {
    [yearKey: number]: {
      [quarterKey: number]: {
        value: number;
      };
    };
  } = {};

  for (
    let i = 0;
    i < datesWithQuarters.length - (currentMonthDate.getMonth() + 1);
    i++
  ) {
    const dateWithQuarter = datesWithQuarters[i];
    const yearKey = dateWithQuarter.date.getFullYear();
    const quarterKey = dateWithQuarter.quarter;
    const quarterInRange = quartersByYearRange[yearKey];
    const saleCountsValue = salecounts[dateWithQuarter.index];

    if (quarterInRange) {
      const yearInRange = quarterInRange[quarterKey];
      if (yearInRange) {
        yearInRange.value += saleCountsValue;
      } else {
        quarterInRange[quarterKey] = {
          value: saleCountsValue,
        };
      }
    } else {
      quartersByYearRange[yearKey] = {
        [quarterKey]: {
          value: saleCountsValue,
        },
      };
    }
  }

  let saleCountsTotalAverage = 0;

  const salecountsByQuarterYear: {
    year: number;
    quarter: number;
    salecounts: number;
  }[] = [];

  Object.keys(quartersByYearRange).forEach((year) => {
    let yearValue = 0;

    Object.keys(quartersByYearRange[+year]).forEach((quarter) => {
      const curr = quartersByYearRange[+year][+quarter];
      salecountsByQuarterYear.push({
        quarter: +quarter,
        year: +year,
        salecounts: curr.value,
      });
      yearValue += curr.value;
    });

    saleCountsTotalAverage += yearValue / 4;
  });

  const highest = salecountsByQuarterYear.reduce((a, b) =>
    a.salecounts > b.salecounts ? a : b
  );

  const lowest = salecountsByQuarterYear.reduce((a, b) =>
    a.salecounts < b.salecounts ? a : b
  );

  const lowestCurrentQuarterDelta = calculateDeltaPercentage(
    currentQuarterPropertiesSold,
    lowest.salecounts
  );

  const lowestCurrentQuarterDeltaChange =
    lowestCurrentQuarterDelta === 0
      ? 'the same'
      : lowestCurrentQuarterDelta > 0
      ? 'stronger'
      : 'weaker';

  const highestCurrentQuarterDelta = calculateDeltaPercentage(
    currentQuarterPropertiesSold,
    highest.salecounts
  );

  const highestCurrentQuarterDeltaChange =
    highestCurrentQuarterDelta === 0
      ? 'the same'
      : highestCurrentQuarterDelta > 0
      ? 'stronger'
      : 'weaker';

  const totalAverageCurrentSalesCountDiff = Math.abs(
    currentQuarterPropertiesSold - saleCountsTotalAverage
  );

  return {
    average: saleCountsTotalAverage ?? 0,
    averageDiff: totalAverageCurrentSalesCountDiff ?? 0,
    change:
      highest.year === currentMonthDate.getFullYear() &&
      highest.quarter === getQuarter(currentMonthDate)
        ? 'highest'
        : lowest.year === currentMonthDate.getFullYear() &&
          lowest.quarter === getQuarter(currentMonthDate)
        ? 'lowest'
        : 'between',
    deltaByMonth: {
      from: lastQuarterPropertiesSold,
      to: currentQuarterPropertiesSold,
      change: lastCurrentQuarterPropertiesSoldDeltaChange,
    },
    deltaBy5YearLow: {
      from: {
        quarter: `Q${lowest.quarter} ${lowest.year}`,
        salecounts: lowest.salecounts,
      },
      amount: lowestCurrentQuarterDelta ?? 0,
      change: lowestCurrentQuarterDeltaChange,
    },
    deltaBy5YearHigh: {
      from: {
        quarter: `Q${highest.quarter} ${highest.year}`,
        salecounts: highest.salecounts,
      },
      amount: highestCurrentQuarterDelta ?? 0,
      change: highestCurrentQuarterDeltaChange,
    },
    chart: {
      data: salecountsByQuarterYear.map((curr) => {
        return {
          date: `Q${curr.quarter} ${curr.year}`,
          value: curr.salecounts,
        };
      }),
    },
  };
};
