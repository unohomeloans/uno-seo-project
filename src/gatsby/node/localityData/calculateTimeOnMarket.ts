import { IDSStats, TimeOnMarket } from '../../../types/HousePrices.types';
import calculateDeltaPercentage from '../../../utils/localityData/calculateDeltaPercentage';
import getLast12MonthsValues from '../../../utils/localityData/getLast12MonthsValues';
import parseChartData from '../../../utils/localityData/parseChartData';
import dayjs from 'dayjs';

export default (
  data: Pick<IDSStats, 'dates'> & Pick<IDSStats, 'tom'>
): TimeOnMarket => {
  const last12MonthDates = getLast12MonthsValues(data.dates);
  const last12MonthTOMs = getLast12MonthsValues(data.tom);
  const currentAndLast12MonthDates = [
    ...last12MonthDates,
    data.dates[data.dates.length - 1],
  ];
  const currentAndLast12MonthTOMs = [
    ...last12MonthTOMs,
    data.tom[data.tom.length - 1],
  ];

  const allTOMs = currentAndLast12MonthDates.map((date, i) => {
    return {
      date,
      tom: currentAndLast12MonthTOMs[i],
    };
  });

  const currentMonthTOM =
    currentAndLast12MonthTOMs[currentAndLast12MonthTOMs.length - 1];

  const lastMonthTOM =
    currentAndLast12MonthTOMs[currentAndLast12MonthTOMs.length - 2];

  const lastCurrentTOMDelta = calculateDeltaPercentage(
    currentMonthTOM,
    lastMonthTOM
  );

  const lastCurrentTOMDeltaChange =
    lastCurrentTOMDelta === 0
      ? 'the same'
      : lastCurrentTOMDelta > 0
      ? 'up'
      : 'down';

  const months: {
    index: number;
    date: string;
    tom: number;
    change: 'up' | 'down' | '';
  }[] = [];

  for (let i = 0; i < allTOMs.length - 1; i++) {
    const curr = allTOMs[i];
    const next = allTOMs[i + 1];

    if (curr.tom >= next.tom) {
      months.push({
        index: i,
        date: curr.date,
        tom: curr.tom,
        change: 'down',
      });
    } else if (curr.tom <= next.tom) {
      months.push({
        index: i,
        date: curr.date,
        tom: curr.tom,
        change: 'up',
      });
    }
  }

  const change: 'up' | 'down' | '' = months[months.length - 1].change;

  let numberOfConsecutiveMonths = 0;
  let sinceConsecutiveMonth = '';

  for (let i = months.length - 1; i >= 0; i--) {
    const curr = months[i];
    if (curr.change === change) {
      numberOfConsecutiveMonths++;
    } else {
      sinceConsecutiveMonth = months[i + 1].date;
      break;
    }
  }

  if (numberOfConsecutiveMonths > 1 && !sinceConsecutiveMonth) {
    sinceConsecutiveMonth = months[0].date;
  }

  return {
    change,
    consecutiveMonths: numberOfConsecutiveMonths,
    sinceConsecutiveMonth: sinceConsecutiveMonth
      ? dayjs(sinceConsecutiveMonth).format('MMMM YYYY')
      : '',
    deltaByMonth: {
      change: lastCurrentTOMDeltaChange,
      from: lastMonthTOM,
      to: currentMonthTOM,
    },
    chart: {
      data: parseChartData(last12MonthDates, last12MonthTOMs),
    },
  };
};
