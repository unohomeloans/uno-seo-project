import {
  IDSStats,
  VendorExpectationError,
} from '../../../types/HousePrices.types';
import getLast12MonthsValues from '../../../utils/localityData/getLast12MonthsValues';
import parseChartData from '../../../utils/localityData/parseChartData';

export default (
  data: Pick<IDSStats, 'dates'> & Pick<IDSStats, 'vee'>
): VendorExpectationError => {
  const last12MonthDates = getLast12MonthsValues(data.dates);
  const last12MonthVEEs = getLast12MonthsValues(data.vee);

  const currentMonthVEE = data.vee[data.vee.length - 1];

  return {
    change: currentMonthVEE > 0 ? 'more' : 'less',
    currentMonth: currentMonthVEE,
    chart: {
      data: parseChartData(last12MonthDates, last12MonthVEEs),
    },
  };
};
