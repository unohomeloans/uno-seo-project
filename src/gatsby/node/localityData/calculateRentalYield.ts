import { IDSStats, RentalYield } from '../../../types/HousePrices.types';
import calculateDeltaPercentage from '../../../utils/localityData/calculateDeltaPercentage';
import getLast12MonthsValues from '../../../utils/localityData/getLast12MonthsValues';
import dayjs from 'dayjs';
import parseChartData from '../../../utils/localityData/parseChartData';

export default (
  data: Pick<IDSStats, 'dates'> & Pick<IDSStats, 'yields'>
): RentalYield => {
  const last12MonthDates = getLast12MonthsValues(data.dates);
  const last12MonthYields = getLast12MonthsValues(data.yields);

  const currentAndLast12MonthDates = [
    ...last12MonthDates,
    data.dates[data.dates.length - 1],
  ];
  const currentAndLast12MonthYields = [
    ...last12MonthYields,
    data.yields[data.yields.length - 1],
  ];

  const allYields = currentAndLast12MonthDates.map((date, i) => {
    return {
      date,
      yield: currentAndLast12MonthYields[i],
    };
  });

  const currentMonth =
    currentAndLast12MonthYields[currentAndLast12MonthYields.length - 1];

  const highest = allYields.reduce((a, b) => (a.yield > b.yield ? a : b));
  const lowest = allYields.reduce((a, b) => (a.yield < b.yield ? a : b));

  const lowestCurrentMonthDelta = calculateDeltaPercentage(
    currentMonth,
    lowest.yield
  );

  const lowestCurrentMonthDeltaChange =
    lowestCurrentMonthDelta === 0
      ? 'the same'
      : lowestCurrentMonthDelta > 0
      ? 'up'
      : 'down';

  const highestCurrentMonthDelta = calculateDeltaPercentage(
    currentMonth,
    highest.yield
  );

  const highestCurrentMonthDeltaChange =
    highestCurrentMonthDelta === 0
      ? 'the same'
      : highestCurrentMonthDelta > 0
      ? 'up'
      : 'down';

  const months: {
    index: number;
    date: string;
    yield: number;
    change: 'increased' | 'decreased' | '';
  }[] = [];

  for (let i = 0; i < allYields.length - 1; i++) {
    const curr = allYields[i];
    const next = allYields[i + 1];

    if (curr.yield >= next.yield) {
      months.push({
        index: i,
        date: curr.date,
        yield: curr.yield,
        change: 'decreased',
      });
    } else if (curr.yield <= next.yield) {
      months.push({
        index: i,
        date: curr.date,
        yield: curr.yield,
        change: 'increased',
      });
    }
  }

  const change: 'increased' | 'decreased' | '' =
    months[months.length - 1].change;

  let numberOfConsecutiveMonths = 0;

  for (let i = months.length - 1; i >= 0; i--) {
    const curr = months[i];
    if (curr.change === change) {
      numberOfConsecutiveMonths++;
    } else {
      break;
    }
  }

  return {
    change,
    currentMonth,
    consecutiveMonths: numberOfConsecutiveMonths,
    deltaBy12MonthHigh: {
      from: {
        date: dayjs(highest.date),
        yield: highest.yield,
      },
      amount: highestCurrentMonthDelta ?? 0,
      change: highestCurrentMonthDeltaChange,
    },
    deltaBy12MonthLow: {
      from: {
        date: dayjs(lowest.date),
        yield: lowest.yield,
      },
      amount: lowestCurrentMonthDelta ?? 0,
      change: lowestCurrentMonthDeltaChange,
    },
    chart: {
      data: parseChartData(last12MonthDates, last12MonthYields),
    },
  };
};
