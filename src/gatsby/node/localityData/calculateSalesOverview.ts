import { IDSStats, SalesOverview } from '../../../types/HousePrices.types';
import calculateDeltaPercentage from '../../../utils/localityData/calculateDeltaPercentage';
import getQuarterlySaleCounts from './calculateSaleCounts';
import { todayDate } from '../createHousePricesPages';

export default (data: IDSStats): SalesOverview => {
  const currentMonthIndex = data.dates.length - 1;
  const currentMonthDate = new Date(data.dates[currentMonthIndex]);
  const lastMonthIndex = data.dates.length - 2;
  const startOfYearIndex = data.dates.findIndex((year) =>
    year.startsWith(`${todayDate.year()}-01`)
  );

  const currentMonthPropertyValue = data.price[currentMonthIndex];
  const lastMonthPropertyValue = data.price[lastMonthIndex];
  const yearPropertyValue = data.price[startOfYearIndex];

  const lastCurrentMonthPropertyValueDelta = calculateDeltaPercentage(
    currentMonthPropertyValue,
    lastMonthPropertyValue
  );
  const lastCurrentMonthPropertyValueDeltaChange =
    lastCurrentMonthPropertyValueDelta === 0
      ? 'the same'
      : lastCurrentMonthPropertyValueDelta > 0
      ? 'increased'
      : 'decreased';

  const yearCurrentMonthPropertyValueDelta = calculateDeltaPercentage(
    currentMonthPropertyValue,
    yearPropertyValue
  );
  const yearCurrentMonthPropertyValueChange =
    yearCurrentMonthPropertyValueDelta === 0
      ? 'the same'
      : yearCurrentMonthPropertyValueDelta > 0
      ? 'increased'
      : 'decreased';

  const currentMonthRent = data.rents[currentMonthIndex];
  const lastMonthRent = data.rents[lastMonthIndex];
  const lastCurrentMonthRentDelta = calculateDeltaPercentage(
    currentMonthRent,
    lastMonthRent
  );
  const lastCurrentMonthRentDeltaChange =
    lastCurrentMonthRentDelta === 0
      ? 'the same'
      : lastCurrentMonthRentDelta > 0
      ? 'up'
      : 'down';

  const currentMonthYield = data.yields[currentMonthIndex];
  const lastMonthYield = data.yields[lastMonthIndex];
  const lastCurrentMonthYieldDelta = calculateDeltaPercentage(
    currentMonthYield,
    lastMonthYield
  );

  const lastCurrentMonthYieldDeltaChange =
    lastCurrentMonthYieldDelta === 0
      ? 'the same'
      : lastCurrentMonthYieldDelta > 0
      ? 'increasing'
      : 'decreasing';

  const quarterlySaleCounts = getQuarterlySaleCounts(
    data.dates,
    currentMonthDate,
    data.salecounts
  );

  const totalCurrentQuarterPropertiesSoldDelta = calculateDeltaPercentage(
    quarterlySaleCounts.deltaByMonth.to,
    quarterlySaleCounts.average
  );

  return {
    medianPropertyValue: currentMonthPropertyValue,
    deltaByMonth: {
      amount: lastCurrentMonthPropertyValueDelta ?? 0,
      change: lastCurrentMonthPropertyValueDeltaChange,
    },
    deltaByYear: {
      amount: yearCurrentMonthPropertyValueDelta ?? 0,
      change: yearCurrentMonthPropertyValueChange,
    },
    rentalDemandChangeByMonth: lastCurrentMonthRentDeltaChange,
    yieldsDeltaByMonth: {
      change: lastCurrentMonthYieldDeltaChange,
      from: lastMonthYield,
      to: currentMonthYield,
    },
    numPropertiesSoldDeltaByQuarter: quarterlySaleCounts.deltaByMonth,
    numPropertiesSoldDeltaByFiveYearQuarters: {
      change:
        totalCurrentQuarterPropertiesSoldDelta ===
        quarterlySaleCounts.deltaByMonth.to
          ? 'the same'
          : totalCurrentQuarterPropertiesSoldDelta > 0
          ? 'up'
          : 'down',
      from: quarterlySaleCounts.average,
      indication:
        totalCurrentQuarterPropertiesSoldDelta ===
        quarterlySaleCounts.deltaByMonth.to
          ? 'no change in'
          : totalCurrentQuarterPropertiesSoldDelta > 0
          ? 'stronger'
          : 'weaker',
    },
  };
};
