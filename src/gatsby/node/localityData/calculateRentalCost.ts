import { IDSStats, RentalCost } from '../../../types/HousePrices.types';
import calculateDeltaPercentage from '../../../utils/localityData/calculateDeltaPercentage';
import getLast12MonthsValues from '../../../utils/localityData/getLast12MonthsValues';
import dayjs from 'dayjs';
import parseChartData from '../../../utils/localityData/parseChartData';

export default (
  data: Pick<IDSStats, 'dates'> & Pick<IDSStats, 'rents'>
): RentalCost => {
  const currentAndLast12MonthDates = getLast12MonthsValues(data.dates);
  currentAndLast12MonthDates.push(data.dates[data.dates.length - 1]);
  const currentAndLast12MonthRents = getLast12MonthsValues(data.rents);
  currentAndLast12MonthRents.push(data.rents[data.rents.length - 1]);

  const allMonthRentsDates = currentAndLast12MonthDates.map((date, i) => {
    return {
      date,
      rent: currentAndLast12MonthRents[i],
    };
  });
  const currentMonth = allMonthRentsDates[allMonthRentsDates.length - 1];
  const lastMonth = allMonthRentsDates[allMonthRentsDates.length - 2];

  const highest = allMonthRentsDates.reduce((a, b) =>
    a.rent > b.rent ? a : b
  );
  const lowest = allMonthRentsDates.reduce((a, b) => (a.rent < b.rent ? a : b));

  const lastCurrentMonthDelta = calculateDeltaPercentage(
    currentMonth.rent,
    lastMonth.rent
  );

  const lastCurrentMonthDeltaChange =
    lastCurrentMonthDelta === 0
      ? 'the same'
      : lastCurrentMonthDelta > 0
      ? 'up'
      : 'down';

  const lowestCurrentMonthDelta = calculateDeltaPercentage(
    currentMonth.rent,
    lowest.rent
  );

  const lowestCurrentMonthDeltaChange =
    lowestCurrentMonthDelta === 0
      ? 'the same'
      : lowestCurrentMonthDelta > 0
      ? 'up'
      : 'down';

  const highestCurrentMonthDelta = calculateDeltaPercentage(
    currentMonth.rent,
    highest.rent
  );

  const highestCurrentMonthDeltaChange =
    highestCurrentMonthDelta === 0
      ? 'the same'
      : highestCurrentMonthDelta > 0
      ? 'up'
      : 'down';

  const last12MonthDates = getLast12MonthsValues(data.dates);
  const last12MonthRents = getLast12MonthsValues(data.rents);

  return {
    currentMonth: currentMonth.rent,
    change:
      highest.date === currentMonth.date
        ? 'highest'
        : lowest.date === currentMonth.date
        ? 'lowest'
        : 'between',
    deltaByMonth: {
      from: lastMonth.rent,
      amount: lastCurrentMonthDelta ?? 0,
      change: lastCurrentMonthDeltaChange,
    },
    deltaBy12MonthHigh: {
      from: {
        date: dayjs(highest.date),
        rent: highest.rent,
      },
      amount: highestCurrentMonthDelta ?? 0,
      change: highestCurrentMonthDeltaChange,
    },
    deltaBy12MonthLow: {
      from: {
        date: dayjs(lowest.date),
        rent: lowest.rent,
      },
      amount: lowestCurrentMonthDelta ?? 0,
      change: lowestCurrentMonthDeltaChange,
    },
    chart: {
      data: parseChartData(last12MonthDates, last12MonthRents),
    },
  };
};
