import { IDSStats, MedianPrice } from '../../../types/HousePrices.types';
import getLast12MonthsValues from '../../../utils/localityData/getLast12MonthsValues';
import parseChartData from '../../../utils/localityData/parseChartData';

export default (
  data: Pick<IDSStats, 'dates'> & Pick<IDSStats, 'price'>
): MedianPrice => {
  const lastMonthIndex = data.dates.length - 2;
  return {
    lastMonthPrice: data.price[lastMonthIndex],
    chart: {
      data: parseChartData(
        getLast12MonthsValues(data.dates),
        getLast12MonthsValues(data.price)
      ),
    },
  };
};
