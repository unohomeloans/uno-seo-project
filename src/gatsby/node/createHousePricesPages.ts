import axios from 'axios';
import dayjs from 'dayjs';
import qs from 'qs';
import { resolve } from 'path';
import { CreatePageArgs } from '../../utils/createPagesUtil';
import {
  HousePricesGraphQL,
  AddedLocalities,
  IDSResponse,
  HouseUnitCalculatedStats,
} from '../../types/HousePrices.types';
import slugify from '../../utils/slugify';
import calculateRentalYield from './localityData/calculateRentalYield';
import calculateSalesOverview from './localityData/calculateSalesOverview';
import calculateRentalCost from './localityData/calculateRentalCost';
import calculateMedianPrice from './localityData/calculateMedianPrice';
import calculateSaleCounts from './localityData/calculateSaleCounts';
import calculateTimeOnMarket from './localityData/calculateTimeOnMarket';
import calculateVendorExpectationError from './localityData/calculateVendorExpectationError';
import { GatsbyCache } from 'gatsby';

const IDSAxios = axios.create({
  baseURL: process.env.GATSBY_IDS_ENDPOINT,
  headers: {
    'x-origin-from': 'MOBY-ORIGIN',
    'x-api-key': process.env.GATSBY_IDS_API_KEY,
  },
});

export const todayDate = dayjs();
const endMonth = todayDate.get('date') > 24 ? 1 : 2;
export const endDate = todayDate.subtract(endMonth, 'month');
export const startDate = endDate.subtract(5, 'year').subtract(1, 'month');

const HOUSE_PRICES_TEMPLATE_PATH = './src/templates/HousePrices.tsx';
const LOCALITY_TEMPLATE_PATH = './src/templates/Locality.tsx';

const getIDSData = (
  suburbPostcode: string
): Promise<HouseUnitCalculatedStats | boolean> => {
  return new Promise((resolve) => {
    setTimeout(async () => {
      try {
        const res = await IDSAxios.get<IDSResponse>(
          '/indices/monthly/locality',
          {
            params: {
              start: startDate.format('YYYY-MM-DD'),
              end: endDate.format('YYYY-MM-DD'),
              property_type: ['house', 'unit'],
              stat: ['tom', 'price', 'vee', 'yields', 'rents', 'salecounts'],
              level: suburbPostcode,
            },
            paramsSerializer: (params) =>
              qs.stringify(params, { arrayFormat: 'repeat' }),
          }
        );

        const houseData = res.data.house.series[suburbPostcode];
        const unitData = res.data.unit.series[suburbPostcode];
        const currentMonthDate = new Date(
          res.data.dates[res.data.dates.length - 1]
        );

        const houseSalesOverview = calculateSalesOverview({
          dates: res.data.dates,
          ...houseData,
        });
        const unitSalesOverview = calculateSalesOverview({
          dates: res.data.dates,
          ...unitData,
        });

        const houseMedianPrice = calculateMedianPrice({
          dates: res.data.dates,
          price: houseData.price,
        });
        const unitMedianPrice = calculateMedianPrice({
          dates: res.data.dates,
          price: unitData.price,
        });

        const houseRentalCost = calculateRentalCost({
          dates: res.data.dates,
          rents: houseData.rents,
        });
        const unitRentalCost = calculateRentalCost({
          dates: res.data.dates,
          rents: unitData.rents,
        });

        const houseRentalYield = calculateRentalYield({
          dates: res.data.dates,
          yields: houseData.yields,
        });
        const unitRentalYield = calculateRentalYield({
          dates: res.data.dates,
          yields: unitData.yields,
        });

        const houseSaleCounts = calculateSaleCounts(
          res.data.dates,
          currentMonthDate,
          houseData.salecounts
        );
        const unitSaleCounts = calculateSaleCounts(
          res.data.dates,
          currentMonthDate,
          unitData.salecounts
        );

        const houseTimeOnMarket = calculateTimeOnMarket({
          dates: res.data.dates,
          tom: houseData.tom,
        });
        const unitTimeOnMarket = calculateTimeOnMarket({
          dates: res.data.dates,
          tom: unitData.tom,
        });

        const houseVendorExpectationError = calculateVendorExpectationError({
          dates: res.data.dates,
          vee: houseData.vee,
        });
        const unitVendorExpectationError = calculateVendorExpectationError({
          dates: res.data.dates,
          vee: unitData.vee,
        });

        return resolve({
          house: {
            salesOverview: houseSalesOverview,
            medianPrice: houseMedianPrice,
            rentalCost: houseRentalCost,
            rentalYield: houseRentalYield,
            saleCounts: houseSaleCounts,
            timeOnMarket: houseTimeOnMarket,
            vendorExpectationError: houseVendorExpectationError,
          },
          unit: {
            salesOverview: unitSalesOverview,
            medianPrice: unitMedianPrice,
            rentalCost: unitRentalCost,
            rentalYield: unitRentalYield,
            saleCounts: unitSaleCounts,
            timeOnMarket: unitTimeOnMarket,
            vendorExpectationError: unitVendorExpectationError,
          },
        });
      } catch (error) {
        return resolve(false);
      }
    }, 8000);
  });
};

export default async (
  { actions, graphql, reporter }: CreatePageArgs,
  cache: GatsbyCache
): Promise<void> => {
  const result = await graphql<HousePricesGraphQL>(`
    {
      allDataJson {
        edges {
          node {
            data {
              lga_name
              broker_name
              broker_image_url
              broker_utm_param
              lga_code
              postcode
              suburb
              state
            }
          }
        }
      }
    }
  `);

  if (result.errors || !result.data) {
    throw result.errors || new Error('No data found');
  }

  const edges = (result.data.allDataJson || {}).edges || [];

  if (!edges || edges.length === 0) {
    reporter.info('No suburb data found');
    return;
  }

  const addedLocalities: AddedLocalities = {
    state: [],
    postcode: [],
    lga: [],
    suburb: [],
  };

  const [lgaBrokers, localities] = edges;

  for (const locality of localities.node.data) {
    if (!locality) {
      reporter.warn(`No locality found`);
      break;
    }

    const lgaCode = locality.lga_code;
    const postcode = locality.postcode;
    const state = locality.state;
    const suburb = locality.suburb;

    // Add unique Suburbs...
    if (
      addedLocalities.suburb.findIndex(
        (addedSuburb) =>
          addedSuburb.suburb === suburb &&
          addedSuburb.postcode === postcode &&
          addedSuburb.lga_code === lgaCode &&
          addedSuburb.state === state
      ) === -1
    ) {
      addedLocalities.suburb.push(locality);
    }

    // Add unique Postcodes...
    if (
      addedLocalities.postcode.findIndex(
        (addedPostcode) =>
          addedPostcode.postcode === postcode &&
          addedPostcode.lga_code === lgaCode &&
          addedPostcode.state === state
      ) === -1
    ) {
      addedLocalities.postcode.push(locality);
    }

    // Add unique LGAs...
    if (
      addedLocalities.lga.findIndex(
        (createdLGA) =>
          createdLGA.lga_code === lgaCode && createdLGA.state === state
      ) === -1
    ) {
      addedLocalities.lga.push(locality);
    }

    // Add unique states...
    if (!addedLocalities.state.includes(state)) {
      addedLocalities.state.push(state);
    }
  }

  // Create State pages...
  addedLocalities.state.forEach((state) => {
    const path = `/${slugify(state)}/`;
    actions.createPage({
      path,
      component: resolve(HOUSE_PRICES_TEMPLATE_PATH),
      context: {
        level: 'state',
        state,
        lgaLinks: addedLocalities.lga
          .filter((createdLga) => createdLga.state === state)
          .sort((lgaPageA, lgaPageB) =>
            lgaPageA.lga_name.localeCompare(lgaPageB.lga_name)
          ),
      },
    });
  });

  // Create LGA pages...
  addedLocalities.lga.forEach((lgaLocality) => {
    const path = `/${slugify(lgaLocality.state)}/${slugify(
      lgaLocality.lga_name
    )}/`;
    actions.createPage({
      path,
      component: resolve(HOUSE_PRICES_TEMPLATE_PATH),
      context: {
        level: 'lga',
        state: lgaLocality.state,
        lga: lgaLocality.lga_name,
        postcodeLinks: addedLocalities.postcode
          .filter(
            (createdPostcode) =>
              createdPostcode.lga_code === lgaLocality.lga_code &&
              createdPostcode.state === lgaLocality.state
          )
          .sort(
            (postcodePageA, postcodePageB) =>
              postcodePageA.postcode - postcodePageB.postcode
          ),
      },
    });
  });

  // Create Postcode pages...
  addedLocalities.postcode.forEach((postcodeLocality) => {
    const path = `/${slugify(postcodeLocality.state)}/${slugify(
      postcodeLocality.lga_name
    )}/${postcodeLocality.postcode}/`;

    actions.createPage({
      path,
      component: resolve(HOUSE_PRICES_TEMPLATE_PATH),
      context: {
        level: 'postcode',
        state: postcodeLocality.state,
        lga: postcodeLocality.lga_name,
        postcode: postcodeLocality.postcode,
        suburbLinks: addedLocalities.suburb
          .filter(
            (createdSuburb) =>
              createdSuburb.postcode === postcodeLocality.postcode &&
              createdSuburb.lga_code === postcodeLocality.lga_code &&
              createdSuburb.state === postcodeLocality.state
          )
          .sort((suburbPageA, suburbPageB) =>
            suburbPageA.suburb.localeCompare(suburbPageB.suburb)
          ),
      },
    });
  });

  const indexLimit = addedLocalities.suburb.length;

  for (let i = 0; i < indexLimit; i++) {
    reporter.info(
      `[SEO Project]: ${Math.round((i / indexLimit) * 100)}%. ${
        indexLimit - i
      } pages remaining...`
    );
    const locality = addedLocalities.suburb[i];
    const path = `/${slugify(locality.state)}/${slugify(locality.lga_name)}/${
      locality.postcode
    }/${slugify(locality.suburb)}/`;
    const suburbPostcode = `${locality.suburb.toUpperCase()}-${
      locality.postcode.toString().length === 3
        ? `0${locality.postcode}`
        : locality.postcode
    }`;

    const cacheKey = suburbPostcode + endDate.format('MM-YYYY');

    const cachedDataString = await cache.get(cacheKey);

    let data;

    if (!cachedDataString) {
      data = await getIDSData(suburbPostcode);
      if (!data) {
        reporter.info(
          '[SEO Project]: Failed to get data for ' + suburbPostcode
        );
        continue;
      }
      reporter.info(
        '[SEO Project - Cache]: Saving ' + suburbPostcode + ' to cache'
      );
      await cache.set(cacheKey, JSON.stringify(data).trim());
    } else {
      reporter.info(
        '[SEO Project - Cache]: Found cache data for ' + suburbPostcode
      );
      data = JSON.parse(cachedDataString);
    }

    actions.createPage({
      path,
      component: resolve(LOCALITY_TEMPLATE_PATH),
      context: {
        level: 'locality',
        state: locality.state,
        lga: locality.lga_name,
        postcode: locality.postcode,
        suburb: locality.suburb,
        date: endDate,
        lastMonthDate: endDate.subtract(1, 'month'),
        lastYearDate: startDate,
        house: data.house,
        unit: data.unit,
        broker: lgaBrokers.node.data.find(
          (lgaBroker) => lgaBroker.lga_name === locality.lga_name
        ),
      },
    });
  }
};
