import React, { useEffect, useState } from 'react';
import dayjs from 'dayjs';
import { ResponsiveContainer, BarChart, XAxis, YAxis, Bar } from 'recharts';
import styled from 'styled-components';
import Heading from '../components/HousePrices/Heading/Heading';
import Layout from '../components/Layout';
import SEO from '../components/SEO';
import theme from '../styles/theme';
import { CalculatedStats, LocalityProps } from '../types/HousePrices.types';
import ordinalSuffix from '../utils/ordinalSuffix';
import { Link } from 'gatsby';

const SubheadingWrapper = styled.section`
  margin-bottom: ${theme.spacing.spacingFive};
  text-align: center;
  width: 550px;
  max-width: 85%;
  display: block;
  margin: 0 auto;

  h3 {
    font-size: ${theme.text.textFive};
  }
`;

const HouseUnitsTabsWrapper = styled.section`
  margin-bottom: ${theme.spacing.spacingFive};
  width: 100%;
  max-width: 100%;
  text-align: center;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  margin-top: ${theme.spacing.spacingFive};

  button {
    background: none;
    border: none;
    padding: 1rem 0.5rem;
    width: 150px;
    font-family: ${theme.text.textBlack};
    font-size: ${theme.text.textFive};
    color: ${theme.colors.colorPrimary};
    &:hover,
    &.selected {
      background: #e7f7f5;
      border-radius: 5px;
    }
  }
`;

const ContentWrapper = styled.article`
  margin: 0 auto;
  padding-left: 0.9375rem;
  padding-right: 0.9375rem;
  max-width: 85rem;
  h3 {
    font-family: ${theme.text.textBlack};
    font-size: ${theme.text.textFive};
    margin-bottom: 2rem;
  }
  p {
    font-size: ${theme.text.fontSizeSmaller};
    margin-bottom: 0.8rem;
  }
  p:last-child {
    margin-bottom: 2rem;
  }
`;

const ChartWrapper = styled.figure`
  text-align: center;
  margin: 0;
  margin-bottom: 5rem;
  margin-top: 1.5rem;
  height: 500px;
  figcaption {
    font-family: ${theme.text.textBlack};
    font-size: ${theme.text.textFive};
    margin-bottom: 0.8rem;
  }
  div {
    margin-top: 2rem;
  }
`;

const BrokerFooter = styled.section<{ showFooter: boolean }>`
  position: fixed;
  z-index: 2;
  bottom: ${({ showFooter }) => (showFooter ? 0 : -500 + 'px')};
  left: 0;
  right: 0;
  margin: 0;
  padding: 0;
  background-color: #2C2F55;
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.25);
  border-radius: 10px 10px 0px 0px;
  height: 130px;
  transition: bottom 0.5s ease;
  display: flex;
  justify-content: space-between;

  @media (max-width: 772px) {
    flex-direction: column;
    height: auto;
  }

  a {
    margin: 1.5rem;
    @media (max-width: 772px) {
      margin: 0;
    }
  }

  aside {
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 1.5rem;
    p {
      color: white;
      margin-top: auto;
      margin-bottom: auto;
      margin-left: 1rem;
      font-size: ${theme.text.textFive};
      @media (max-width: 772px) {
        font-size: ${theme.text.textThreee};
      }
    }
    img {
      width: 80px;
      height: 100%;
    }
  }
`;

const Locality: React.FC<LocalityProps> = ({ pageContext }) => {
  const { lga, postcode, state, suburb, broker } = pageContext;
  const latestDate = dayjs(pageContext.date);
  const lastMonthDate = dayjs(pageContext.lastMonthDate);
  const lastMonth = lastMonthDate.format('MMMM');
  const latestMonth = latestDate.format('MMMM');
  const latestYear = latestDate.format('YYYY');
  const latestDay = latestDate.format('DD');

  const { format: currencyFormat } = new Intl.NumberFormat('en-AU', {
    style: 'currency',
    currency: 'AUD',
    minimumFractionDigits: 0,
    maximumFractionDigits: 0,
  });

  const [data, setData] = useState<CalculatedStats>(pageContext.house);

  const capitalisedSuburb = suburb.replace(/(^\w{1})|(\s{1}\w{1})/g, (match) =>
    match.toUpperCase()
  );

  const seoTerm = `${capitalisedSuburb} ${lga}, ${postcode} ${state}`;

  const [selectedTab, setSelectedTab] = useState<string>('houses');
  const [showFooter, setShowFooter] = useState<boolean>(false);

  const selectedTabDisplay =
    selectedTab
      .slice(0, selectedTab.length - 1)
      .charAt(0)
      .toUpperCase() + selectedTab.slice(1, selectedTab.length - 1);

  const lowercasedSelectedTabDisplay = selectedTabDisplay.toLowerCase();

  const handleTabSwitch = (e: React.MouseEvent<HTMLButtonElement>) => {
    const { id } = e.currentTarget;
    setSelectedTab(e.currentTarget.id);
    if (id === 'houses') {
      setData(pageContext.house);
    } else {
      setData(pageContext.unit);
    }
  };

  const handleScroll = () => {
    const currentScrollPosition = window.scrollY;
    const scrollHeight =
      document.documentElement.scrollHeight -
      document.documentElement.clientHeight;
    const depth = (currentScrollPosition / scrollHeight) * 100;
    setShowFooter(depth > 7);
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);

    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  return (
    <Layout>
      <SEO
        title={`House prices in ${seoTerm} | uno Home Loans`}
        description={`House prices and statistics in ${seoTerm}. Covering stats such as Vendor Expectation Error (VEE), Time on Market (TOM), Salecounts, Yields, Price and Rent.`}
      />
      <Heading level="locality" {...pageContext} />
      <SubheadingWrapper>
        <h2>
          <strong>
            {capitalisedSuburb} Property Data {latestMonth} {latestYear}
          </strong>
        </h2>
        <h3>
          Updated {latestDay} {latestMonth} {latestYear}
        </h3>
        <HouseUnitsTabsWrapper>
          <button
            id="houses"
            onClick={handleTabSwitch}
            {...(selectedTab === 'houses' && { className: 'selected' })}>
            Houses
          </button>
          <button
            id="units"
            onClick={handleTabSwitch}
            {...(selectedTab === 'units' && { className: 'selected' })}>
            Units
          </button>
        </HouseUnitsTabsWrapper>
      </SubheadingWrapper>
      <ContentWrapper>
        <section id="property-sales-overview">
          <h3>{capitalisedSuburb} Property Sales Overview</h3>
          <p>
            The median property value in {capitalisedSuburb} for {latestMonth}{' '}
            is {currencyFormat(data.salesOverview.medianPropertyValue)}.
          </p>
          <p>
            {selectedTabDisplay}s have {data.salesOverview.deltaByMonth.change}{' '}
            by{' '}
            {data.salesOverview.deltaByMonth.amount
              ? data.salesOverview.deltaByMonth.amount.toFixed(2)
              : 0}
            % for the month and {data.salesOverview.deltaByYear.change}{' '}
            {data.salesOverview.deltaByYear.amount
              ? data.salesOverview.deltaByYear.amount.toFixed(2)
              : 0}
            % for the year.
          </p>
          <p>
            Rental demand is {data.salesOverview.rentalDemandChangeByMonth} with
            yields {data.salesOverview.yieldsDeltaByMonth.change} in February to{' '}
            {data.salesOverview.yieldsDeltaByMonth.to
              ? data.salesOverview.yieldsDeltaByMonth.to.toFixed(2)
              : 0}
            %, from{' '}
            {data.salesOverview.yieldsDeltaByMonth.from
              ? data.salesOverview.yieldsDeltaByMonth.from.toFixed(2)
              : 0}
            % in January.
          </p>
          <p>
            The number of properties sold this quarter is{' '}
            {data.salesOverview.numPropertiesSoldDeltaByQuarter.change} on last
            quarter from{' '}
            {data.salesOverview.numPropertiesSoldDeltaByQuarter.from} to{' '}
            {data.salesOverview.numPropertiesSoldDeltaByQuarter.to}. This is{' '}
            {data.salesOverview.numPropertiesSoldDeltaByFiveYearQuarters.change}{' '}
            on the 5 year quarterly average of{' '}
            {data.salesOverview.numPropertiesSoldDeltaByFiveYearQuarters.from
              ? data.salesOverview.numPropertiesSoldDeltaByFiveYearQuarters.from.toFixed(
                  0
                )
              : 0}{' '}
            indicating{' '}
            {
              data.salesOverview.numPropertiesSoldDeltaByFiveYearQuarters
                .indication
            }{' '}
            demand.
          </p>
        </section>
        <section id="median-price">
          <h3>
            What&apos;s the median {selectedTabDisplay} price in{' '}
            {capitalisedSuburb} in {latestMonth} {latestYear}?
          </h3>
          <p>
            The median property value for {lowercasedSelectedTabDisplay}s in{' '}
            {capitalisedSuburb} is{' '}
            {currencyFormat(data.salesOverview.medianPropertyValue)}.
          </p>
          <p>
            This is{' '}
            {data.salesOverview.deltaByMonth.change === 'increased'
              ? 'an increase'
              : 'a decrease'}{' '}
            of{' '}
            {data.salesOverview.deltaByMonth.amount
              ? data.salesOverview.deltaByMonth.amount.toFixed(2)
              : 0}
            % ({currencyFormat(data.medianPrice.lastMonthPrice)}) from last
            month.
          </p>
          <ChartWrapper>
            <figcaption>Median price looking back 12 months</figcaption>
            <ResponsiveContainer width="100%" height="100%">
              <BarChart
                data={data.medianPrice.chart.data}
                margin={{ left: 30, right: 30 }}
                barCategoryGap="20%">
                <XAxis dataKey="date" />
                <YAxis
                  domain={['auto', 'auto']}
                  tickFormatter={(value) => currencyFormat(value)}
                />
                <Bar dataKey="value" fill={theme.colors.colorPrimary} />
              </BarChart>
            </ResponsiveContainer>
          </ChartWrapper>
        </section>
        <section id="rental-cost">
          <h3>
            How much does it cost to rent a {selectedTabDisplay} in{' '}
            {capitalisedSuburb}?
          </h3>
          {data.rentalCost.change === 'between' && (
            <p>
              Rent prices in {capitalisedSuburb} went{' '}
              {data.rentalCost.deltaByMonth.change} by{' '}
              {data.rentalCost.deltaByMonth.amount
                ? data.rentalCost.deltaByMonth.amount.toFixed(2)
                : 0}
              % in {latestMonth} to{' '}
              {currencyFormat(data.rentalCost.currentMonth)} compared to{' '}
              {currencyFormat(data.rentalCost.deltaByMonth.from)} in {lastMonth}
              , {data.rentalCost.deltaBy12MonthHigh.change} from a 12 month high
              of {currencyFormat(data.rentalCost.deltaBy12MonthHigh.from.rent)}{' '}
              in{' '}
              {dayjs(data.rentalCost.deltaBy12MonthHigh.from.date).format(
                'MMMM YYYY'
              )}{' '}
              and {data.rentalCost.deltaBy12MonthLow.change} from a 12 month low
              of {currencyFormat(data.rentalCost.deltaBy12MonthLow.from.rent)}{' '}
              in{' '}
              {dayjs(data.rentalCost.deltaBy12MonthLow.from.date).format(
                'MMMM YYYY'
              )}
              .
            </p>
          )}
          {data.rentalCost.change === 'highest' && (
            <>
              <p>
                Rent prices in {capitalisedSuburb} are the highest they&apos;ve
                been in 12 months at{' '}
                {currencyFormat(data.rentalCost.currentMonth)}.
              </p>
              <p>
                This is up{' '}
                {data.rentalCost.deltaBy12MonthLow.amount
                  ? data.rentalCost.deltaBy12MonthLow.amount.toFixed(2)
                  : 0}
                % from a 12 month low of{' '}
                {currencyFormat(data.rentalCost.deltaBy12MonthLow.from.rent)} in{' '}
                {dayjs(data.rentalCost.deltaBy12MonthLow.from.date).format(
                  'MMMM YYYY'
                )}{' '}
                and a{' '}
                {data.rentalCost.deltaByMonth.amount
                  ? data.rentalCost.deltaByMonth.amount.toFixed(2)
                  : 0}
                % {data.rentalCost.deltaByMonth.change} from last month.
              </p>
            </>
          )}
          {data.rentalCost.change === 'lowest' && (
            <>
              <p>
                Rent prices in {capitalisedSuburb} are the lowest they&apos;ve
                been in 12 months at{' '}
                {currencyFormat(data.rentalCost.currentMonth)}.
              </p>
              <p>
                This is down{' '}
                {data.rentalCost.deltaBy12MonthHigh.amount
                  ? data.rentalCost.deltaBy12MonthHigh.amount.toFixed(2)
                  : 0}
                % from a 12 month high of{' '}
                {currencyFormat(data.rentalCost.deltaBy12MonthHigh.from.rent)}{' '}
                in{' '}
                {dayjs(data.rentalCost.deltaBy12MonthHigh.from.date).format(
                  'MMMM YYYY'
                )}{' '}
                and a {data.rentalCost.deltaByMonth.change} from last month.
              </p>
            </>
          )}
          <ChartWrapper>
            <figcaption>Rental cost looking back 12 months</figcaption>
            <ResponsiveContainer width="100%" height="100%">
              <BarChart
                data={data.rentalCost.chart.data}
                margin={{ left: 30, right: 30 }}
                barCategoryGap="20%">
                <XAxis dataKey="date" />
                <YAxis
                  domain={['auto', 'auto']}
                  tickFormatter={(value) => currencyFormat(value)}
                />
                <Bar dataKey="value" fill={theme.colors.colorPrimary} />
              </BarChart>
            </ResponsiveContainer>
          </ChartWrapper>
        </section>
        <section id="rental-yield">
          <h3>What&apos;s the current rental yield in {capitalisedSuburb}?</h3>
          {data.rentalYield.consecutiveMonths > 1 ? (
            <p>
              Rental yields for {lowercasedSelectedTabDisplay}s in{' '}
              {capitalisedSuburb} have {data.rentalYield.change} in{' '}
              {latestMonth} for the{' '}
              {ordinalSuffix(data.rentalYield.consecutiveMonths)} consecutive
              month.
            </p>
          ) : (
            <p>
              Rental yields for {lowercasedSelectedTabDisplay}s in{' '}
              {capitalisedSuburb} is {data.rentalYield.currentMonth}%.
            </p>
          )}
          <p>
            This is {data.rentalYield.deltaBy12MonthLow.change} from a 12 month
            low of {data.rentalYield.deltaBy12MonthLow.from.yield}% in{' '}
            {dayjs(data.rentalYield.deltaBy12MonthLow.from.date).format(
              'MMMM YYYY'
            )}{' '}
            and {data.rentalYield.deltaBy12MonthHigh.change} from a 12 month
            high of {data.rentalYield.deltaBy12MonthHigh.from.yield}% in{' '}
            {dayjs(data.rentalYield.deltaBy12MonthHigh.from.date).format(
              'MMMM YYYY'
            )}
            .
          </p>
          <ChartWrapper>
            <figcaption>Rental yield looking back 12 months</figcaption>
            <ResponsiveContainer width="100%" height="100%">
              <BarChart
                data={data.rentalYield.chart.data}
                margin={{ left: 30, right: 30 }}
                barCategoryGap="20%">
                <XAxis dataKey="date" />
                <YAxis domain={['dataMin - 0.5', 'auto']} unit="%" />
                <Bar dataKey="value" fill={theme.colors.colorPrimary} />
              </BarChart>
            </ResponsiveContainer>
          </ChartWrapper>
        </section>
        <section id="sale-counts">
          <h3>How many properties sold in {capitalisedSuburb} in February?</h3>
          <p>
            Total sales volume in {capitalisedSuburb} is{' '}
            {data.saleCounts.deltaByMonth.to}, which is{' '}
            {data.saleCounts.deltaByMonth.change} from{' '}
            {data.saleCounts.deltaByMonth.from} in the previous quarter.
          </p>
          {data.saleCounts.change === 'between' && (
            <p>
              This is{' '}
              {data.saleCounts.deltaBy5YearLow.change === 'the same'
                ? 'the same as '
                : `${
                    data.saleCounts.deltaBy5YearLow.amount
                      ? data.saleCounts.deltaBy5YearLow.amount.toFixed(2)
                      : 0
                  }% ${data.saleCounts.deltaBy5YearLow.change} than`}{' '}
              the weakest quarter in the last 5 years which was{' '}
              {data.saleCounts.deltaBy5YearLow.from.quarter} with{' '}
              {data.saleCounts.deltaBy5YearLow.from.salecounts} sales and{' '}
              {data.saleCounts.deltaBy5YearHigh.amount
                ? data.saleCounts.deltaBy5YearHigh.amount.toFixed(2)
                : 0}
              % {data.saleCounts.deltaBy5YearHigh.change} than the strongest
              quarter in the past 5 years which saw{' '}
              {data.saleCounts.deltaBy5YearHigh.from.salecounts} sales in{' '}
              {data.saleCounts.deltaBy5YearHigh.from.quarter}.
            </p>
          )}
          {data.saleCounts.change === 'highest' && (
            <p>
              This is the highest volume of sales for any quarter in the last 5
              years! Up{' '}
              {data.saleCounts.deltaBy5YearLow.amount
                ? data.saleCounts.deltaBy5YearLow.amount.toFixed(2)
                : 0}
              % from a low of {data.saleCounts.deltaBy5YearLow.from.salecounts}{' '}
              sales in {data.saleCounts.deltaBy5YearLow.from.quarter} and{' '}
              {data.saleCounts.averageDiff} above average.
            </p>
          )}
          {data.saleCounts.change === 'lowest' && (
            <p>
              This is the lowest volume of sales we&apos;ve seen in{' '}
              {capitalisedSuburb} in the past 5 years. Down{' '}
              {data.saleCounts.deltaBy5YearHigh.amount
                ? data.saleCounts.deltaBy5YearHigh.amount.toFixed(2)
                : 0}
              % from a high of{' '}
              {data.saleCounts.deltaBy5YearHigh.from.salecounts} sales in{' '}
              {data.saleCounts.deltaBy5YearHigh.from.quarter}.
            </p>
          )}
          <ChartWrapper>
            <figcaption>
              Quarterly volume of sales looking back 5 years
            </figcaption>
            <ResponsiveContainer width="100%" height="100%">
              <BarChart
                data={data.saleCounts.chart.data}
                margin={{ left: 30, right: 30 }}
                barCategoryGap="20%">
                <XAxis dataKey="date" />
                <YAxis />
                <Bar dataKey="value" fill={theme.colors.colorPrimary} />
              </BarChart>
            </ResponsiveContainer>
          </ChartWrapper>
        </section>
        <section id="time-on-market">
          <h3>
            How long did it take to sell a property in {capitalisedSuburb} in
            February?
          </h3>
          <p>
            {data.timeOnMarket.deltaByMonth.to} days is the average time on
            market in {capitalisedSuburb} in {latestMonth}, which is{' '}
            {data.timeOnMarket.deltaByMonth.change} from{' '}
            {data.timeOnMarket.deltaByMonth.from} in {lastMonth}
            {data.timeOnMarket.consecutiveMonths > 1 &&
              ` and has been trending ${data.timeOnMarket.change} since ${data.timeOnMarket.sinceConsecutiveMonth}`}
            .
          </p>
          {data.timeOnMarket.change === 'up' && (
            <p>
              This indicates a competitive market for buyers where the more
              organised you are the better chance you have of securing.
            </p>
          )}
          {data.timeOnMarket.change === 'down' && (
            <p>
              This can indicate a buyers market where sellers are finding it
              harder to make a sale.
            </p>
          )}
          <ChartWrapper>
            <figcaption>
              Number of days on market looking back 12 months
            </figcaption>
            <ResponsiveContainer width="100%" height="100%">
              <BarChart
                data={data.timeOnMarket.chart.data}
                margin={{ left: 30, right: 30 }}
                barCategoryGap="20%">
                <XAxis dataKey="date" />
                <YAxis domain={['dataMin - 2', 'auto']} />
                <Bar dataKey="value" fill={theme.colors.colorPrimary} />
              </BarChart>
            </ResponsiveContainer>
          </ChartWrapper>
        </section>
        <section id="vendor-expectation-error">
          <h3>
            Are properties selling below or above the listing price in{' '}
            {capitalisedSuburb}?
          </h3>
          <p>
            {latestMonth} sees the vendor expectation error for{' '}
            {capitalisedSuburb} at {data.vendorExpectationError.currentMonth}%
            which means, on average, places sold for{' '}
            {data.vendorExpectationError.currentMonth}%{' '}
            {data.vendorExpectationError.change} than the listing price.
          </p>
          {data.vendorExpectationError.change === 'less' && (
            <p>This is a great sign for buyers looking to negotiate.</p>
          )}
          {data.vendorExpectationError.change === 'more' && (
            <p>
              This is handy to know as a buyer to set your expectation and to
              give you a more realistic view of what you might expect properties
              to sell for on auction day.
            </p>
          )}
          <p>
            It’s worth noting that in a hot market you might want to set your
            price expectations even higher than the{' '}
            {data.vendorExpectationError.currentMonth}% as things can move
            pretty quickly.
          </p>
          <ChartWrapper>
            <figcaption>
              Vendor expectation error looking back 12 months
            </figcaption>
            <ResponsiveContainer width="100%" height="100%">
              <BarChart
                data={data.vendorExpectationError.chart.data}
                margin={{ left: 30, right: 30 }}
                barCategoryGap="20%">
                <XAxis dataKey="date" />
                <YAxis domain={['auto', 'auto']} unit="%" />
                <Bar dataKey="value" fill={theme.colors.colorPrimary} />
              </BarChart>
            </ResponsiveContainer>
          </ChartWrapper>
        </section>
      </ContentWrapper>
      <BrokerFooter showFooter={showFooter}>
        <aside>
          <img
            alt={`Profile photo of ${broker.broker_name}`}
            src={broker.broker_image_url}
          />
          <p>
            Thinking of buying in {capitalisedSuburb}?{' '} Schedule a quick call with uno to understand your borrowing power or options

          </p>
        </aside>
        <Link
          to={`https://calendly.com/uno-customer-care/uno-quick-call-houseprices`}
          className="button button--md button--primary">
          Schedule a Call
        </Link>
      </BrokerFooter>
    </Layout>
  );
};

export default Locality;
