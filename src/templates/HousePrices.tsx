import React from 'react';
import Indexer from '../components/HousePrices/Indexer';
import { HousePricesProps } from '../types/HousePrices.types';

const HousePrices: React.FC<HousePricesProps> = ({ pageContext }) => {
  return <Indexer {...pageContext} />;
};

export default HousePrices;
