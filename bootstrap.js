/* eslint-disable @typescript-eslint/no-var-requires */
const esbuild = require('esbuild');

const bootstrapGatsbyFile = (inputTSFilePath, isNode) => {
  const paths = inputTSFilePath.split('/');

  const outputPath = `./gatsby-${paths[paths.length - 1].split('.')[0]}-out.js`;

  esbuild.buildSync({
    entryPoints: [inputTSFilePath],
    bundle: true,
    platform: isNode ? 'node' : 'browser',
    outfile: outputPath,
  });

  return outputPath;
};

module.exports = bootstrapGatsbyFile;
