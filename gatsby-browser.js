/* eslint-disable @typescript-eslint/no-var-requires */
import queryString from 'querystring';
import segment from './src/utils/segment';
const siteConfig = require('./config/site-config.js');

const DEFAULT_IMAGE = 'https://cdn.unohomeloans.com.au/images/uno-logo.svg';
const DEFAULT_DATA = {
  type: '',
  id: '',
  author: siteConfig.author || '',
  category: '',
  image: '',
  publishedAt: '',
  updatedAt: '',
};

const getUTMParams = (locationSearch) => {
  const { utm_campaign, utm_source, utm_medium, utm_term, utm_content } =
    queryString.parse(locationSearch);

  return {
    utm_campaign: utm_campaign,
    utm_source: utm_source,
    utm_medium: utm_medium,
    utm_term: utm_term,
    utm_content: utm_content,
  };
};

const getData = (data) => {
  if (data.post) {
    return {
      ...DEFAULT_DATA,
      type: 'post',
      id: data.post.id,
      author: data.post.author.name,
      category: data.post.categories.map((tag) => tag.title).join(' '),
      image: data.post.mainImage.asset.url,
      publishedAt: data.post.publishedAt,
      updatedAt: data.post._updatedAt,
    };
  } else if (data.category) {
    return {
      ...DEFAULT_DATA,
      type: 'category',
      id: data.category.id,
      image: data.category.categoryImage.asset.url,
    };
  } else if (data.lender) {
    return {
      ...DEFAULT_DATA,
      type: 'lender',
      id: data.lender.id,
      image: DEFAULT_IMAGE,
    };
  } else if (data.page || data.route) {
    return {
      ...DEFAULT_DATA,
      type: 'page',
      id: data.route ? data.route.page.id : data.page.id,
      image: DEFAULT_IMAGE,
    };
  }

  return DEFAULT_DATA;
};

const onRouteUpdate = ({ location, data }) => {
  if (!data) {
    console.error('No data found');
    return;
  }

  segment('page', {
    ...getData(data),
    ...getUTMParams(location.search),
  });

  segment('scroll');

  setTimeout(() => segment('engaged'), 29000);
};

export const wrapPageElement = ({ element, props }) => {
  onRouteUpdate({ location: props.location, data: props.data });
  return element;
};
