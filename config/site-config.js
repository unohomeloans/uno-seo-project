module.exports = {
  pathPrefix: '/',
  title:
    'Active Home Loan Manager | Online Mortgage Broker | We Analyse, We Alert, We Act.',
  description:
    'Ready to win at Home Loans? uno - The Online Mortgage Broker who actively manages your home loan for you. We fight home loan waste for you so you could be saving thousands.',
  siteUrl: 'https://houseprices.unohomeloans.com.au/',
  siteLanguage: 'en',
  author: '@uno',
  siteFBappID: '441858419592511',
  userTwitter: '@unohomeloans',
  facebook: 'https://www.facebook.com/unohomeloans/',
  twitter: 'https://twitter.com/unoHomeLoans',
  menuLinks: [
    {
      name: 'loanScore',
      link: '/loanscore',
    },
    {
      name: 'Newsroom',
      link: '/categories/newsroom',
    },
    {
      name: 'Resources',
      link: '/',
      class: 'has-item',
      items: [
        {
          name: 'Your Borrowing Power',
          link: '/how-much-can-i-borrow',
        },
        {
          name: 'Buying Property',
          link: '/buying-a-house',
        },
        {
          name: 'Refinance Home Loan',
          link: '/buying-a-house',
        },
        {
          name: 'Property Investment',
          link: '/investing',
        },
        {
          name: 'First Home Buyer',
          link: '/first-home-buyer',
        },
        {
          name: 'Best Home Loan Rates',
          link: '/best-home-loan-rates',
        },
        {
          name: 'Our Lenders',
          link: '/lenders',
        },
        {
          name: 'Home Loans Dictionary',
          link: '/categories/glossary/',
        },
        {
          name: 'uno Home Loans Blog',
          link: '/categories/newsroom',
        },
      ],
    },
    {
      name: 'About us',
      link: '/about-us',
      class: 'has-item',
      items: [
        {
          name: 'uno FAQs',
          link: '/help',
        },
        {
          name: 'Customer Stories',
          link: '/categories/customer-stories',
        },
      ],
    },
    {
      name: 'Contact us',
      link: '/contact',
    },
  ],
  footerLinks: [
    {
      uno: [
        {
          name: 'About us',
          link: '/about-us',
        },
        {
          name: 'Contact us',
          link: '/contact',
        },
        {
          name: 'Expert profiles',
          link: '/uno-experts',
        },
        {
          name: 'Join our team',
          link: '/work-at-uno',
        },
        {
          name: 'Media',
          link: '/media',
        },
        {
          name: 'Terms and conditions',
          link: '/terms-and-conditions',
        },
        {
          name: 'AHLM Terms and conditions',
          link: '/active-home-loan-management-terms',
        },
        {
          name: 'Privacy policy',
          link: '/privacy-policy',
        },
        {
          name: 'Credit Guide',
          link: '/uno-credit-guide',
        },
        {
          name: 'Feedback',
          link: 'https://unohomeloans.com.au/home-loans/#feedback',
        },
        {
          name: 'Site Map',
          link: '/site-map',
        },
      ],
      calculators: [
        {
          name: 'How much can I borrow?',
          link: 'https://unohomeloans.com.au/how-much-can-i-borrow/',
        },
        {
          name: 'How much can I save by refinancing?',
          link: 'https://unohomeloans.com.au/home-loans/#refinance-calculator',
        },
        {
          name: 'How much do I need to borrow?',
          link: 'https://unohomeloans.com.au/home-loans/#funds-required-calculator',
        },
      ],
      compare: [
        {
          name: 'Check your loanScore',
          link: '/loanscore',
        },
        {
          name: 'Home loan search',
          link: '/find-me-homeloan',
        },
        {
          name: 'uno home loan lenders',
          link: '/lenders',
        },
      ],
      resources: [
        {
          name: 'Buying property guide',
          link: '/how-much-can-i-borrow',
        },
        {
          name: 'First home buyer guide',
          link: '/buying',
        },
        {
          name: 'Property investment guide',
          link: '/refinance-home-loan',
        },
        {
          name: 'Refinancing guide',
          link: '/investing',
        },
        {
          name: 'Mortgage broker guide',
          link: '/first-home-buyer',
        },
        {
          name: 'Home loan types',
          link: '/best-home-loan-rates',
        },
        {
          name: 'Home loan glossary',
          link: '/categories/glossary',
        },
        {
          name: 'Chat to us',
          link: '/contact',
        },
        {
          name: 'Call us on 133 866',
          link: '/contact',
        },
      ],
    },
  ],
  socialLinks: [
    {
      name: 'Twitter',
      title: 'Connect on twitter',
      link: 'https://twitter.com/unoHomeLoans',
      icon: 'https://cdn.unohomeloans.com.au/images/icon-twitter-white.svg',
    },
    {
      name: 'Facebook',
      title: 'Connect on Facebook',
      link: 'https://www.facebook.com/unohomeloans/',
      icon: 'https://cdn.unohomeloans.com.au/images/icon-facebook-white.svg',
    },
    {
      name: 'Linkedin',
      title: 'Connect on Linkedin',
      link: 'https://www.linkedin.com/company/uno-home-loans?trk=tyah&amp;trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A7945202%2Cidx%3A2-1-2%2CtarId%3A1461801781615%2Ctas%3Auno%20home%20loans',
      icon: 'https://cdn.unohomeloans.com.au/images/icon-linkedin-white.svg',
    },
  ],
  footerCopyright:
    'Planwise AU Pty Ltd trading as uno Home Loans ABN 81 609 882 804 Australian Credit Licence Number 483595. uno values your privacy and last updated our Privacy Policy on 29 January 2020.',
};
