Due to Netlify limitations, the SEO pages must be re-built manually to reflect any new Broker - LGA distribution updates or IDS data updates. It is hosted on a separate domain to unohomeloans.com.au, namely https://houseprices.unohomeloans.com.au.

## Prerequisites

[NodeJS](https://nodejs.org/en/) + [Yarn](https://classic.yarnpkg.com/en/docs/install/) + [AWS CLI](https://aws.amazon.com/cli/) installed.

IAM User - `seo-deploy`.

# Instructions

1. Git clone the uno-seo-project repository

   ```bash
   git clone https://<YourUserName>@bitbucket.org/unohomeloans/uno-seo-project.git
   ```

2. Install all dependencies

   ```bash
   yarn
   ```

3. Build + deploy the project - This will take ~ 4 - 5 days - ensure that the machine has a reliable internet connection and does not stop abruptly and deploy the project

   ```bash
   sh build-only.sh public seo.unohomeloans.com.au E2UH7TFIXGEZUN


   sh deploy-only.sh public seo.unohomeloans.com.au E2UH7TFIXGEZUN
   ```
