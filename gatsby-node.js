/* eslint-disable @typescript-eslint/no-var-requires */
const bootstrap = require('./bootstrap');
const path = bootstrap('./src/gatsby/node/node.ts', true);
module.exports = require(path);
