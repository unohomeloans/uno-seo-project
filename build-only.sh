#!/usr/bin/env bash
# Usage deploy.sh <folder> <bucket_name> <cloudfront_id>
deploy_folder="$1"
s3_bucket_name="$2"
cloudfront_dist_id="$3"

echo "Deploying SEO Project...";

if [ ! $deploy_folder ]; then
echo "Deployment folder cannot be empty."
echo "Usage: deploy.sh <folder> <bucket_name> <cloudfront_id>"
exit 2;
fi

if [ ! $s3_bucket_name ]; then
echo "S3 Bucket Name cannot be empty."
echo "Usage: deploy.sh <folder> <bucket_name> <cloudfront_id>"
exit 2;
fi

if [ ! $cloudfront_dist_id ]; then
echo "Cloudfrint Distribution Id cannot be empty."
echo "Usage: deploy.sh <folder> <bucket_name> <cloudfront_id>"
exit 2;
fi

export GATSBY_SITE_URL=https://houseprices.unohomeloans.com.au/ GATSBY_SEGMENTKEY=Nr7O1Gergf8sogGTskK3iqZAKMXHD8rG GATSBY_PRODUCT_SEARCH_URL=https://api.unohomeloans.com.au/application-api/prod/product-search/json GATSBY_BORROWING_POWER_URL=https://api.unohomeloans.com.au/application-api/qa/calculators/borrowing-power APIKEY=miltontest GATSBY_MY_SANITY_PROJECT_ID="86mn51w1" GATSBY_MY_SANITY_DATASET="staging" GATSBY_MY_SANITY_TOKEN="skyTMH5b87bsx7WPizz1Re5HRpjzr2rqMzL8uYE8yxtCsmJi5Sf40rwD2Fu1zpn1D7EklgEIj08bdAE46IGEQMchFfGlrI0OVWeszkXOqnLLp70YPiujQrlf5vbVHvBpyZJ4btJ2dAL1DYxQL6WuFPEpJZ0qmZWVxfn6i8bhTPc0SOksTDrR" GATSBY_MY_SANITY_LS="https://loanscore.unohomeloans.com.au/" GATSBY_MY_SANITY_CV2="https://app.unohomeloans.com.au/" GATSBY_MY_OPTIMIZELY="5614690270" GATSBY_IDS_ENDPOINT=https://propertysearch.integration.uno/v1/property-search-service GATSBY_IDS_API_KEY=d98fcgTbiidLmSWzpguTyC20r1n0QzfU

yarn build


echo "Finished building SEO Project...";
